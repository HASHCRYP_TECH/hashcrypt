package com.grapes.santelquikeyy.Model;

/**
 * Created by pc on 8/13/2017.
 */

public class ArrayListTempAcSub {

    public int position;
    public String strintCount;

    public ArrayListTempAcSub(int position, String strintCount) {
        this.position = position;
        this.strintCount = strintCount;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getStrintCount() {
        return strintCount;
    }

    public void setStrintCount(String strintCount) {
        this.strintCount = strintCount;
    }
}
