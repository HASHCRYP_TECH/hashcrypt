package com.grapes.santelquikeyy.Model;

/**
 * Created by pc on 7/31/2017.
 */

public class TypeofPrice
{
    String id;
    String title;
    String name;
    String str_number_of_service;
    String str_price;
    int numberType;
    private boolean isSelected;

    public String getTitle() {
        return title;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getStr_price() {
        return str_price;
    }

    public void setStr_price(String str_price) {
        this.str_price = str_price;
    }

    public void setId(String id) {
        this.id = id;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setNumberType(int numberType) {
        this.numberType = numberType;
    }
    public int getNumberType()
    {
        return numberType;
    }
    public boolean setSelected(boolean selected) {
        isSelected = selected;
        return selected;
    }
    public boolean isSelected() {
        return isSelected;
    }

    public String getStr_number_of_service() {
        return str_number_of_service;
    }

    public void setStr_number_of_service(String str_number_of_service) {
        this.str_number_of_service = str_number_of_service;
    }
}
