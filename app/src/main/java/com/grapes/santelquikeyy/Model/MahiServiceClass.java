package com.grapes.santelquikeyy.Model;

import java.io.Serializable;

/**
 * Created by pc on 8/13/2017.
 */

public class MahiServiceClass implements Serializable {

    String typeOfAc;

    public String getTypeOfAc() {
        return typeOfAc;
    }

    public void setTypeOfAc(String typeOfAc) {
        this.typeOfAc = typeOfAc;
    }

    public class TypeOfService implements Serializable {
        String serviceType;

        public String getServiceType() {
            return serviceType;
        }

        public void setServiceType(String serviceType) {
            this.serviceType = serviceType;
        }
    }

    public class ServiceDetails implements Serializable{
        String serviceName;
        String servicePrice;
        String serviceTag;

        public ServiceDetails(String serviceName, String servicePrice, String serviceTag) {
            this.serviceName = serviceName;
            this.servicePrice = servicePrice;
            this.serviceTag = serviceTag;
        }

        public String getServiceName() {
            return serviceName;
        }

        public void setServiceName(String serviceName) {
            this.serviceName = serviceName;
        }

        public String getServicePrice() {
            return servicePrice;
        }

        public void setServicePrice(String servicePrice) {
            this.servicePrice = servicePrice;
        }

        public String getServiceTag() {
            return serviceTag;
        }

        public void setServiceTag(String serviceTag) {
            this.serviceTag = serviceTag;
        }
    }

}
