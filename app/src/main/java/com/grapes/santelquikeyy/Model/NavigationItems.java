package com.grapes.santelquikeyy.Model;

/**
 * Created by prats on 5/25/2015.
 */
public class NavigationItems {
    String mTitle;
    String mSubtitle;
    int image;

    public NavigationItems(int home_menu, String title) {
        mTitle = title;
        image=home_menu;
    }

    public String getmTitle() {
        return mTitle;
    }

    public int getImage() {
        return image;
    }
}
