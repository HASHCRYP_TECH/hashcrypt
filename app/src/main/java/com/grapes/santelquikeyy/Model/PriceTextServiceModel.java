package com.grapes.santelquikeyy.Model;

/**
 * Created by pc on 8/11/2017.
 */

public class PriceTextServiceModel
{
    String str_sevice_name;
    String str_price;
    String str_service;


    public String getStr_price() {
        return str_price;
    }

    public String getStr_service() {
        return str_service;
    }

    public String getStr_sevice_name() {
        return str_sevice_name;
    }

    public void setStr_price(String str_price) {
        this.str_price = str_price;
    }

    public void setStr_service(String str_service) {
        this.str_service = str_service;
    }

    public void setStr_sevice_name(String str_sevice_name) {
        this.str_sevice_name = str_sevice_name;
    }
}
