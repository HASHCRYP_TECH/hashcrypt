package com.grapes.santelquikeyy.Model;

/**
 * Created by pc on 8/9/2017.
 */

public class TempAddsres
{
    String str_nick_name;
    String str_address;

    public String getStr_address() {
        return str_address;
    }

    public String getStr_nick_name() {
        return str_nick_name;
    }

    public void setStr_address(String str_address) {
        this.str_address = str_address;
    }

    public void setStr_nick_name(String str_nick_name) {
        this.str_nick_name = str_nick_name;
    }
}
