package com.grapes.santelquikeyy.Model;

/**
 * Created by pc on 7/22/2017.
 */

public class BannerOffers
{
    String id;
    String title;
    String image;


    public String getTitle() {
        return title;
    }
    public String getId() {
        return id;
    }
    public String getImage() {
        return image;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public void setId(String id) {
        this.id = id;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
