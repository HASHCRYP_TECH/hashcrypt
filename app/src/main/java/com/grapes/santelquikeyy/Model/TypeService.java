package com.grapes.santelquikeyy.Model;

/**
 * Created by pc on 7/31/2017.
 */

public class TypeService
{
    String id;
    String name;
    int numberType;
    private boolean isSelected;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getNumberType() {
        return numberType;
    }
    public void setNumberType(int numberType) {
        this.numberType = numberType;
    }
    public void setSelected(boolean selected) {
        isSelected = selected;
    }
    public boolean isSelected() {
        return isSelected;
    }
}
