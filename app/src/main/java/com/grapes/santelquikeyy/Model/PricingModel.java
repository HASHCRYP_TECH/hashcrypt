package com.grapes.santelquikeyy.Model;

/**
 * Created by pc on 8/11/2017.
 */

public class PricingModel
{


    String str_pricing_name;
    String str_pricing_service;
    String str_pricing_price;


    public String getStr_pricing_name() {
        return str_pricing_name;
    }

    public String getStr_pricing_price() {
        return str_pricing_price;
    }

    public String getStr_pricing_service() {
        return str_pricing_service;
    }

    public void setStr_pricing_name(String str_pricing_name) {
        this.str_pricing_name = str_pricing_name;
    }

    public void setStr_pricing_price(String str_pricing_price) {
        this.str_pricing_price = str_pricing_price;
    }

    public void setStr_pricing_service(String str_pricing_service) {
        this.str_pricing_service = str_pricing_service;
    }
}
