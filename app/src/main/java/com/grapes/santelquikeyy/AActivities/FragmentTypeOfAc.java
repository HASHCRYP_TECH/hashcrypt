package com.grapes.santelquikeyy.AActivities;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.grapes.santelquikeyy.Activities.NewACServicesRepairActivity;
import com.grapes.santelquikeyy.Adapter.DesReviewAdapter;
import com.grapes.santelquikeyy.Adapter.OfferAdapter;
import com.grapes.santelquikeyy.Adapter.ProductAdapter;
import com.grapes.santelquikeyy.ItemClickSupport.ItemClickSupport;
import com.grapes.santelquikeyy.Model.BannerOffers;
import com.grapes.santelquikeyy.Model.Offer;
import com.grapes.santelquikeyy.Model.Products;
import com.grapes.santelquikeyy.R;
import com.grapes.santelquikeyy.SeparatorDecoration.SeparatorDecoration;
import com.grapes.santelquikeyy.UtilsApi;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pc on 7/24/2017.
 */

public class FragmentTypeOfAc extends Fragment
{

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view=inflater.inflate(R.layout.frag_type_of_ac,container,false);


        initViews(view);
        return view;
    }

    private void initViews(View view)
    {

    }

}
