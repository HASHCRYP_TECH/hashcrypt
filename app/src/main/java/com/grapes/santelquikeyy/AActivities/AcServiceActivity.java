package com.grapes.santelquikeyy.AActivities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.grapes.santelquikeyy.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class AcServiceActivity extends AppCompatActivity {

    public static RelativeLayout relativeNext;
    public ViewPager verificationViewPager;
    public VerificationPagerAdapter verificationPagerAdapter;
    public static AcServiceActivity acServiceActivity;
    ImageView imgBack;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_service_activity);


        initView();
    }

    private void initView() {




        imgBack = (ImageView) findViewById(R.id.txt_back);

        acServiceActivity = AcServiceActivity.this;
        verificationViewPager = (ViewPager) findViewById(R.id.ac_viewpager);

        relativeNext = (RelativeLayout) findViewById(R.id.relative_schedule_s_service);
        verificationPagerAdapter = new VerificationPagerAdapter(AcServiceActivity.this.getSupportFragmentManager());
        verificationPagerAdapter.addFragment(new FragmentTypeOfAc(), "type of ac");
        verificationPagerAdapter.addFragment(new FragmentSubTypeOfAc(), "Sub type");
        verificationPagerAdapter.addFragment(new FragmentTypeOfPrice(), "type of price");
        verificationPagerAdapter.addFragment(new FragmentAddAddress(), "add address");
        verificationPagerAdapter.addFragment(new FragmentPricingDetails(), "pricing_details");

        verificationViewPager.setAdapter(verificationPagerAdapter);

        verificationViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        setClickEvent();
    }

    private void setClickEvent() {
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void addFragmentInView(final Fragment fragment, final String string) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                verificationPagerAdapter.addFragment(fragment, string);
                verificationViewPager.setAdapter(verificationPagerAdapter);
            }
        });
    }

    public void getPositionofFragment(Fragment fragment) {
//        verificationPagerAdapter.getCount();
        Log.e("pageposition", ": " + verificationViewPager.getCurrentItem());
        verificationViewPager.setCurrentItem(verificationViewPager.getCurrentItem() + 1);
    }


    public class VerificationPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        VerificationPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
//            return mFragmentTitleList.get(position);
            return null;
        }

        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }
    }

    @Override
    public void onBackPressed() {
        if (verificationViewPager.getCurrentItem() > 0) {
            verificationViewPager.setCurrentItem(verificationViewPager.getCurrentItem() - 1);
        } else {
            super.onBackPressed();
        }
    }
}