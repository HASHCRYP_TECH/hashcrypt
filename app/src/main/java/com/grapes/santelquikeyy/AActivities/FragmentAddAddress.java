package com.grapes.santelquikeyy.AActivities;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.grapes.santelquikeyy.R;

/**
 * Created by pc on 7/24/2017.
 */

public class FragmentAddAddress extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_add_address, container, false);


        initViews(view);
        return view;
    }

    private void initViews(View view) {

        AcServiceActivity.relativeNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //AcServiceActivity.acServiceActivity.addFragmentInView(new FragmentSubTypeOfAc(), "AC");
                AcServiceActivity.acServiceActivity.getPositionofFragment(FragmentAddAddress.this);
            }
        });

    }

}
