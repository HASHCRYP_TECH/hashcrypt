package com.grapes.santelquikeyy.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.grapes.santelquikeyy.Model.TypeService;
import com.grapes.santelquikeyy.Model.TypeofPrice;
import com.grapes.santelquikeyy.R;

import java.util.ArrayList;

/**
 * Created by pc on 7/31/2017.
 */

public class TypeAdapter extends BaseAdapter {
    ArrayList<TypeService>arrayList;
    Context context;

    private static LayoutInflater inflater=null;
    public TypeAdapter(Context context, ArrayList<TypeService> arrayList) {
        // TODO Auto-generated constructor stub
        this.arrayList=arrayList;
        this. context=context;
        inflater = (LayoutInflater)context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return arrayList.size();
    }
    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }
    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }
    public class Holder
    {
        TextView tv;
        CheckBox checkbox_1;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final Holder holder=new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.type_text_check_layout, null);
        holder.tv=(TextView) rowView.findViewById(R.id.txt_option_one);
        holder.checkbox_1=(CheckBox)rowView.findViewById(R.id.checkbox_1);
        String str_name=arrayList.get(position).getName();
        String str_2=arrayList.get(position).getName();
        String desiredString = str_2.substring(0,2);
        String str= str_name.substring(1);
        holder.tv.setText(desiredString+"\n"+str);
        holder.checkbox_1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                int Position = (Integer) buttonView.getTag();
                arrayList.get(Position).setSelected(buttonView.isChecked());
            }
        });
        return rowView;
    }
}
