package com.grapes.santelquikeyy.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.grapes.santelquikeyy.Model.PricingModel;
import com.grapes.santelquikeyy.R;

import java.util.ArrayList;

/**
 * Created by pc on 8/11/2017.
 */

public class PricingAdapter extends BaseAdapter {
    Context context;
    private static LayoutInflater inflater=null;
   private ArrayList<PricingModel>arrayList;

    public PricingAdapter(Context applicationContext, ArrayList<PricingModel> arrayList)
    {
        this.context=applicationContext;
        this.arrayList=arrayList;
        inflater = ( LayoutInflater )context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        TextView txt_title,txt_count_service,txt_count_price;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder=new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.pricing_text_list_item_layout, null);
        holder.txt_count_price=(TextView)rowView.findViewById(R.id.txt_count_price);
        holder.txt_count_service=(TextView)rowView.findViewById(R.id.txt_count_service);
        holder.txt_title=(TextView) rowView.findViewById(R.id.txt_title);

        holder.txt_title.setText(arrayList.get(position).getStr_pricing_name());
        holder.txt_count_service.setText(arrayList.get(position).getStr_pricing_service());
        holder.txt_count_price.setText(arrayList.get(position).getStr_pricing_price());
        return rowView;
    }
}
