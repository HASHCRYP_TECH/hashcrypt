package com.grapes.santelquikeyy.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.grapes.santelquikeyy.Activities.NewACServicesRepairActivity;
import com.grapes.santelquikeyy.Activities.Type_of_Ac_Activity;
import com.grapes.santelquikeyy.Model.PriceTextServiceModel;
import com.grapes.santelquikeyy.Model.TypeService;
import com.grapes.santelquikeyy.R;

import java.util.ArrayList;

/**
 * Created by pc on 7/31/2017.
 */

public class TypeRecAdapter extends RecyclerView.Adapter<TypeRecAdapter.ViewHolder> {
    private ArrayList<TypeService> android;
    private Context context;
    private ArrayList<Integer> arrayList = new ArrayList<>();

    public TypeRecAdapter(Context context, ArrayList<TypeService> android) {
        this.android = android;
        this.context = context;
    }

    @Override
    public TypeRecAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.type_text_check_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final TypeRecAdapter.ViewHolder viewHolder, final int i) {

        // viewHolder.tv_android.setText(android.get(i).getName());

        boolean check_true_or_false = android.get(i).isSelected();
        viewHolder.checkbox_1.setText(android.get(i).getName());
        viewHolder.checkbox_1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                android.get(viewHolder.getAdapterPosition()).setSelected(isChecked);

                if (isChecked) {
                    arrayList.add(1);

                    Type_of_Ac_Activity.relative_done.setBackgroundResource(R.drawable.rounded_textview_button_black);
                } else {
                    arrayList.remove(0);

                    if (arrayList.size() > 0) {

                    } else {
                        Type_of_Ac_Activity.relative_done.setBackgroundResource(R.drawable.rounded_textview_button_gray);
                    }

                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return android.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_android;
        private CheckBox checkbox_1;

        public ViewHolder(View view) {
            super(view);
            tv_android = (TextView) view.findViewById(R.id.txt_option_one);
            checkbox_1 = (CheckBox) view.findViewById(R.id.checkbox_1);
        }
    }
}
