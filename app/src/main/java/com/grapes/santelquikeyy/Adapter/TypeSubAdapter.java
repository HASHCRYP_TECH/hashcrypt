package com.grapes.santelquikeyy.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.grapes.santelquikeyy.Model.TypeOfServicesTwo;
import com.grapes.santelquikeyy.Model.TypeofPrice;
import com.grapes.santelquikeyy.R;

import java.security.KeyRep;
import java.util.ArrayList;

/**
 * Created by pc on 7/31/2017.
 */

public class TypeSubAdapter extends BaseAdapter {
    ArrayList<TypeOfServicesTwo>arrayList;
    Context context;

    private static LayoutInflater inflater=null;
    public TypeSubAdapter(Context context, ArrayList<TypeOfServicesTwo> arrayList) {
        // TODO Auto-generated constructor stub
        this.arrayList=arrayList;
        this. context=context;
        inflater = (LayoutInflater)context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return arrayList.size();
    }
    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }
    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }
    public class Holder
    {
        TextView tv;
        CheckBox checkbox_1;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder=new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.type_price_text_check_layout, null);
        holder.tv=(TextView) rowView.findViewById(R.id.txt_option_one);
        holder.checkbox_1=(CheckBox)rowView.findViewById(R.id.checkbox_1);
        holder.tv.setText(arrayList.get(position).getName());
        holder.checkbox_1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            }
        });
        return rowView;
    }
}
