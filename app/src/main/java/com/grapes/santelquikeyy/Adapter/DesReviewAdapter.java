package com.grapes.santelquikeyy.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.grapes.santelquikeyy.Model.Offer;
import com.grapes.santelquikeyy.R;

import java.util.List;

/**
 * Created by pc on 7/21/2017.
 */

public class DesReviewAdapter extends RecyclerView.Adapter<DesReviewAdapter.MyViewHolder> {

    private List<Offer> list_offer_image;
    private Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView img_offer;
        public TextView txt_title,txt_des;

        public MyViewHolder(View view) {
            super(view);
            img_offer=(ImageView)view.findViewById(R.id.img_product);
            txt_title=(TextView)view.findViewById(R.id.txt_title);
            txt_des=(TextView)view.findViewById(R.id.txt_des);
        }
    }
    public DesReviewAdapter(Context context, List<Offer> list_offer_image)
    {
        this.list_offer_image = list_offer_image;
        this.mContext=context;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.review_image_text_layout, parent, false);
        return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.img_offer.setBackgroundResource(list_offer_image.get(position).getOfferimage());
        Typeface tf_title = Typeface.createFromAsset(mContext.getAssets(),"font/OpenSans-Bold.ttf");
        Typeface tfdes = Typeface.createFromAsset(mContext.getAssets(),"font/OpenSans-Regular.ttf");
        holder.txt_title.setTypeface(tf_title,Typeface.BOLD);
        holder.txt_des.setTypeface(tfdes);
    }
    @Override
    public int getItemCount() {
        return list_offer_image.size();
    }
}
