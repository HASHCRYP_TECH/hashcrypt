package com.grapes.santelquikeyy.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.grapes.santelquikeyy.Activities.NewACServicesRepairActivity;
import com.grapes.santelquikeyy.Model.PriceTextServiceModel;
import com.grapes.santelquikeyy.R;

import java.util.List;

public abstract class RadioAdapter<T> extends RecyclerView.Adapter<RadioAdapter.ViewHolder> {
    public int mSelectedItem = -1;
    public List<T> mItems;
    private Context mContext;


    public RadioAdapter(Context context, List<T> items) {
        mContext = context;
        mItems = items;
    }

    @Override
    public void onBindViewHolder(RadioAdapter.ViewHolder viewHolder, final int i) {
        viewHolder.mRadio.setChecked(i == mSelectedItem);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        final View view = inflater.inflate(R.layout.view_item, viewGroup, false);
        return new ViewHolder(view);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public RadioButton mRadio;

        //public TextView mText;
        public ViewHolder(final View inflate) {
            super(inflate);
            // mText = (TextView) inflate.findViewById(R.id.text);
            mRadio = (RadioButton) inflate.findViewById(R.id.radio);
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyItemRangeChanged(0, mItems.size());
                    NewACServicesRepairActivity.typeofPriceArrayList.get(0).setSelected(true);
                    NewACServicesRepairActivity.strradiobutton_value = mRadio.getText().toString();
//                    if (NewACServicesRepairActivity.check)
//                    {
//                        NewACServicesRepairActivity.check=false;
//                        NewACServicesRepairActivity.arrayList_show_display_service_price_2.add(NewACServicesRepairActivity.strradiobutton_value);
//                    }else {
                        if (NewACServicesRepairActivity.str_count.equals("1"))
                        {
                            String strname=NewACServicesRepairActivity.arrayList_show_display_service_name_1.get(0).getStr_sevice_name();
                            PriceTextServiceModel priceTextServiceModel=new PriceTextServiceModel();
                            priceTextServiceModel.setStr_price(NewACServicesRepairActivity.strradiobutton_value);
                            priceTextServiceModel.setStr_sevice_name(strname);
                            NewACServicesRepairActivity.arrayList_show_display_service_name_1.set(0,priceTextServiceModel);

                            if (NewACServicesRepairActivity.arrayList_show_display_service_price_2.size() > 0) {
                                String str_value = NewACServicesRepairActivity.arrayList_show_display_service_price_2.get(0);
                                if (NewACServicesRepairActivity.strradiobutton_value.equals(str_value)) {

                                } else {
                                    NewACServicesRepairActivity.arrayList_show_display_service_price_2.set(0, NewACServicesRepairActivity.strradiobutton_value);

                                }
                            } else {
                                NewACServicesRepairActivity.arrayList_show_display_service_price_2.add(NewACServicesRepairActivity.strradiobutton_value);
                            }
                        } else if (NewACServicesRepairActivity.str_count.equals("2")) {

                            String strname=NewACServicesRepairActivity.arrayList_show_display_service_name_1.get(1).getStr_sevice_name();
                            PriceTextServiceModel priceTextServiceModel=new PriceTextServiceModel();
                            priceTextServiceModel.setStr_price(NewACServicesRepairActivity.strradiobutton_value);
                            priceTextServiceModel.setStr_sevice_name(strname);
                            NewACServicesRepairActivity.arrayList_show_display_service_name_1.set(1,priceTextServiceModel);
                            if (NewACServicesRepairActivity.arrayList_show_display_service_price_2.size() > 0) {
                                String str_value = NewACServicesRepairActivity.arrayList_show_display_service_price_2.get(0);
                                if (NewACServicesRepairActivity.strradiobutton_value.equals(str_value)) {

                                } else {
                                    NewACServicesRepairActivity.arrayList_show_display_service_price_2.set(0, NewACServicesRepairActivity.strradiobutton_value);

                                }
                            } else {
                                NewACServicesRepairActivity.arrayList_show_display_service_price_2.add(NewACServicesRepairActivity.strradiobutton_value);
                            }

                        } else if (NewACServicesRepairActivity.str_count.equals("3")) {
                            String strname=NewACServicesRepairActivity.arrayList_show_display_service_name_1.get(2).getStr_sevice_name();
                            PriceTextServiceModel priceTextServiceModel=new PriceTextServiceModel();
                            priceTextServiceModel.setStr_price(NewACServicesRepairActivity.strradiobutton_value);
                            priceTextServiceModel.setStr_sevice_name(strname);
                            NewACServicesRepairActivity.arrayList_show_display_service_name_1.set(2,priceTextServiceModel);
                            if (NewACServicesRepairActivity.arrayList_show_display_service_price_2.size() > 0) {
                                String str_value = NewACServicesRepairActivity.arrayList_show_display_service_price_2.get(0);
                                if (NewACServicesRepairActivity.strradiobutton_value.equals(str_value)) {

                                } else {
                                    NewACServicesRepairActivity.arrayList_show_display_service_price_2.set(0, NewACServicesRepairActivity.strradiobutton_value);

                                }
                            } else {
                                NewACServicesRepairActivity.arrayList_show_display_service_price_2.add(NewACServicesRepairActivity.strradiobutton_value);
                            }

                        } else if (NewACServicesRepairActivity.str_count.equals("4")) {
                            String strname=NewACServicesRepairActivity.arrayList_show_display_service_name_1.get(3).getStr_sevice_name();
                            PriceTextServiceModel priceTextServiceModel=new PriceTextServiceModel();
                            priceTextServiceModel.setStr_price(NewACServicesRepairActivity.strradiobutton_value);
                            priceTextServiceModel.setStr_sevice_name(strname);
                            NewACServicesRepairActivity.arrayList_show_display_service_name_1.set(3,priceTextServiceModel);
                            if (NewACServicesRepairActivity.arrayList_show_display_service_price_2.size() > 0) {
                                String str_value = NewACServicesRepairActivity.arrayList_show_display_service_price_2.get(0);
                                if (NewACServicesRepairActivity.strradiobutton_value.equals(str_value)) {

                                } else {
                                    NewACServicesRepairActivity.arrayList_show_display_service_price_2.set(0, NewACServicesRepairActivity.strradiobutton_value);

                                }
                            } else {
                                NewACServicesRepairActivity.arrayList_show_display_service_price_2.add(NewACServicesRepairActivity.strradiobutton_value);
                            }

                        }
                    }
               // }
            };
            itemView.setOnClickListener(clickListener);
            mRadio.setOnClickListener(clickListener);
        }
    }
}
