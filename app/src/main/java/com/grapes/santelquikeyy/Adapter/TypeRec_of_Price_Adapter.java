package com.grapes.santelquikeyy.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.grapes.santelquikeyy.Activities.TypeAcSubActivity;
import com.grapes.santelquikeyy.Activities.TypeOfPriceActivity;
import com.grapes.santelquikeyy.Model.TypeOfServicesTwo;
import com.grapes.santelquikeyy.Model.TypeofPrice;
import com.grapes.santelquikeyy.R;

import java.util.ArrayList;

/**
 * Created by pc on 7/31/2017.
 */

public class TypeRec_of_Price_Adapter extends RecyclerView.Adapter<TypeRec_of_Price_Adapter.ViewHolder> {
    private ArrayList<TypeofPrice> android;
    private Context context;
    private ArrayList<Integer> arrayList=new ArrayList<>();
    private static int lastCheckedPos = 0;
    private static CheckBox lastChecked = null;

    public TypeRec_of_Price_Adapter(Context context, ArrayList<TypeofPrice> android) {
        this.android = android;
        this.context = context;
    }
    @Override
    public TypeRec_of_Price_Adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.type_text_check_layout, viewGroup, false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(final TypeRec_of_Price_Adapter.ViewHolder viewHolder, final int i)
    {

       // viewHolder.tv_android.setText(android.get(i).getName());
        if(i == 0 && android.get(0).isSelected() && viewHolder.checkbox_1.isChecked())
        {

            lastChecked = viewHolder.checkbox_1;
            lastCheckedPos = 0;
        }
        viewHolder.checkbox_1.setText(android.get(i).getName());
        viewHolder.checkbox_1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                CheckBox cb = (CheckBox)v;
                int clickedPos = ((Integer)cb.getTag()).intValue();

                if(cb.isChecked())
                {
                    if(lastChecked != null)
                    {
                        lastChecked.setChecked(false);
                        android.get(lastCheckedPos).setSelected(false);
                    }
                    lastChecked = cb;
                    lastCheckedPos = clickedPos;
                }
                else
                    lastChecked = null;

                android.get(clickedPos).setSelected(cb.isChecked());
            }
        });
//        viewHolder.checkbox_1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                android.get(viewHolder.getAdapterPosition()).setSelected(isChecked);
//                if (isChecked)
//                {
//                    arrayList.add(1);
//                    TypeOfPriceActivity.relative_done.setBackgroundResource(R.drawable.rounded_textview_button_black);
//                }else {
//                    if (arrayList.size()>0)
//                    {
//                        arrayList.remove(0);
//                    }
//                    if (arrayList.size()>0)
//                    {
//
//                    }else {
//                        TypeOfPriceActivity.relative_done.setBackgroundResource(R.drawable.rounded_textview_button_gray);
//                    }
//
//                }
//            }
//        });
    }
    @Override
    public int getItemCount() {
        return android.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_android;
        private CheckBox checkbox_1;
        public ViewHolder(View view) {
            super(view);
            tv_android = (TextView) view.findViewById(R.id.txt_option_one);
            checkbox_1=(CheckBox)view.findViewById(R.id.checkbox_1);
        }
    }
}
