package com.grapes.santelquikeyy.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.grapes.santelquikeyy.Activities.NewACServicesRepairActivity;
import com.grapes.santelquikeyy.Activities.TypeAcSubActivity;
import com.grapes.santelquikeyy.Activities.Type_of_Ac_Activity;
import com.grapes.santelquikeyy.Model.ArrayListTempAcSub;
import com.grapes.santelquikeyy.Model.MahiServiceClass;
import com.grapes.santelquikeyy.Model.PriceTextServiceModel;
import com.grapes.santelquikeyy.Model.TypeOfServicesTwo;
import com.grapes.santelquikeyy.Model.TypeService;
import com.grapes.santelquikeyy.R;

import java.util.ArrayList;

/**
 * Created by pc on 7/31/2017.
 */

public class TypeRecSubAdapter extends RecyclerView.Adapter<TypeRecSubAdapter.ViewHolder> {
    private ArrayList<TypeOfServicesTwo> android;
    private Context context;
    private ArrayList<Integer> arrayList = new ArrayList<>();

    public TypeRecSubAdapter(Context context, ArrayList<TypeOfServicesTwo> android) {
        this.android = android;
        this.context = context;
    }

    @Override
    public TypeRecSubAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.type_text_check_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final TypeRecSubAdapter.ViewHolder viewHolder, final int i) {

        //viewHolder.tv_android.setText(android.get(i).getName());

        viewHolder.checkbox_1.setText(android.get(i).getName());
//        viewHolder.checkbox_1.setSelected(android.get(i).isSelected());


       /* if (android.get(i).isSelected()) {
            arrayList.add(1);
            PriceTextServiceModel priceTextServiceModel = new PriceTextServiceModel();
            priceTextServiceModel.setStr_sevice_name(android.get(i).getName());
            NewACServicesRepairActivity.arrayList_service_check.add(android.get(i).getName());
            String str_count = String.valueOf(i + 1);

//                    ArrayListTempAcSub arrayListTempAcSub = new ArrayListTempAcSub(i, str_count);
//                    NewACServicesRepairActivity.arrayList_temp.add(arrayListTempAcSub);
            NewACServicesRepairActivity.arrayList_temp.add(str_count);
            NewACServicesRepairActivity.arrayList_count.add(str_count);
            // NewACServicesRepairActivity.arrayList_show_display_service_name_1.add(priceTextServiceModel);
            TypeAcSubActivity.relative_done.setBackgroundResource(R.drawable.rounded_textview_button_black);
        } else {
            if (NewACServicesRepairActivity.arrayList_show_display_service_name_1.size() > 0) {
                String str_remove = android.get(i).getName();
                // NewACServicesRepairActivity.arrayList_show_display_service_name_1.remove(i);
                NewACServicesRepairActivity.arrayList_service_check.remove(str_remove);
                String str_count = NewACServicesRepairActivity.arrayList_temp.get(i);
//                        String str_count = "0";
//                        for (int j = 0; j < NewACServicesRepairActivity.arrayList_temp.size(); j++) {
//                            if (i == NewACServicesRepairActivity.arrayList_temp.get(j).getPosition()) {
//                                str_count = NewACServicesRepairActivity.arrayList_temp.get(i).getStrintCount();
//                            }
//                        }

                NewACServicesRepairActivity.arrayList_count.remove(str_count);
                if (NewACServicesRepairActivity.arrayList_service_check.size() > 0) {
                    //NewACServicesRepairActivity.arrayList_service_check.remove(i);
                }
                if (NewACServicesRepairActivity.arrayList_count.size() > 0) {

                    //NewACServicesRepairActivity.arrayList_count.remove(i);
                }
            }
            if (arrayList.size() > 0) {
                arrayList.remove(0);
            }
            if (arrayList.size() > 0) {

            } else {
                TypeAcSubActivity.relative_done.setBackgroundResource(R.drawable.rounded_textview_button_gray);
            }


        }*/


        viewHolder.checkbox_1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                android.get(viewHolder.getAdapterPosition()).setSelected(isChecked);

                //((TypeAcSubActivity) context).addremovesubtype(isChecked, i);

                TypeOfServicesTwo typeOfServicesTwo = new TypeOfServicesTwo();
                typeOfServicesTwo.setId(android.get(i).getId());
                typeOfServicesTwo.setName(android.get(i).getName());
                typeOfServicesTwo.setSelected(isChecked);
                typeOfServicesTwo.setNumberType(android.get(i).getNumberType());
                typeOfServicesTwo.setTitle(android.get(i).getTitle());
//                NewACServicesRepairActivity.arrayListTypeOfTwo.set(i, typeOfServicesTwo);
                if (isChecked) {
                    arrayList.add(1);
                    PriceTextServiceModel priceTextServiceModel = new PriceTextServiceModel();
                    priceTextServiceModel.setStr_sevice_name(android.get(i).getName());
                    NewACServicesRepairActivity.arrayList_service_check.add(android.get(i).getName());
                    String str_count = String.valueOf(i + 1);

//                    ArrayListTempAcSub arrayListTempAcSub = new ArrayListTempAcSub(i, str_count);
//                    NewACServicesRepairActivity.arrayList_temp.add(arrayListTempAcSub);
                    NewACServicesRepairActivity.arrayList_temp.add(str_count);
                    NewACServicesRepairActivity.arrayList_count.add(str_count);
                    // NewACServicesRepairActivity.arrayList_show_display_service_name_1.add(priceTextServiceModel);
                    TypeAcSubActivity.relative_done.setBackgroundResource(R.drawable.rounded_textview_button_black);
                } else {
                    if (NewACServicesRepairActivity.arrayList_show_display_service_name_1.size() > 0) {
                        String str_remove = android.get(i).getName();
                        // NewACServicesRepairActivity.arrayList_show_display_service_name_1.remove(i);
                        NewACServicesRepairActivity.arrayList_service_check.remove(str_remove);
                        String str_count = NewACServicesRepairActivity.arrayList_temp.get(i);
//                        String str_count = "0";
//                        for (int j = 0; j < NewACServicesRepairActivity.arrayList_temp.size(); j++) {
//                            if (i == NewACServicesRepairActivity.arrayList_temp.get(j).getPosition()) {
//                                str_count = NewACServicesRepairActivity.arrayList_temp.get(i).getStrintCount();
//                            }
//                        }

                        NewACServicesRepairActivity.arrayList_count.remove(str_count);
                        if (NewACServicesRepairActivity.arrayList_service_check.size() > 0) {
                            //NewACServicesRepairActivity.arrayList_service_check.remove(i);
                        }
                        if (NewACServicesRepairActivity.arrayList_count.size() > 0) {

                            //NewACServicesRepairActivity.arrayList_count.remove(i);
                        }
                    }
                    if (arrayList.size() > 0) {
                        arrayList.remove(0);
                    }
                    if (arrayList.size() > 0) {

                    } else {
                        TypeAcSubActivity.relative_done.setBackgroundResource(R.drawable.rounded_textview_button_gray);
                    }


                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return android.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_android;
        private CheckBox checkbox_1;

        public ViewHolder(View view) {
            super(view);
            tv_android = (TextView) view.findViewById(R.id.txt_option_one);
            checkbox_1 = (CheckBox) view.findViewById(R.id.checkbox_1);
        }
    }
}
