package com.grapes.santelquikeyy.Adapter;

import android.content.Context;

import com.grapes.santelquikeyy.Model.TypeofPrice;

import java.util.List;

public class PersonAdapter extends RadioAdapter<TypeofPrice> {
    public PersonAdapter(Context context, List<TypeofPrice> items) {
        super(context, items);
    }
    @Override
    public void onBindViewHolder(RadioAdapter.ViewHolder viewHolder, final  int i) {
        super.onBindViewHolder(viewHolder, i);
        viewHolder.mRadio.setText(mItems.get(i).getName());


    }
}
