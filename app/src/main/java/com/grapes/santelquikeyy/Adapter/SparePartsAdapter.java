package com.grapes.santelquikeyy.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.grapes.santelquikeyy.Model.BannerOffers;
import com.grapes.santelquikeyy.R;

import java.util.List;

/**
 * Created by pc on 7/21/2017.
 */

public class SparePartsAdapter extends RecyclerView.Adapter<SparePartsAdapter.MyViewHolder> {

    private List<Integer> list_offer_image;
    private Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView img_offer;

        public MyViewHolder(View view) {
            super(view);
            img_offer=(ImageView)view.findViewById(R.id.img_spare_parts);
        }
    }
    public SparePartsAdapter(Context context, List<Integer> list_offer_image)
    {
        this.list_offer_image = list_offer_image;
        this.mContext=context;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.spare_parts_image_layout, parent, false);

        return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Glide.with(mContext).load(list_offer_image.get(position)).fitCenter().into(holder.img_offer);
    }
    @Override
    public int getItemCount() {
        return list_offer_image.size();
    }
}
