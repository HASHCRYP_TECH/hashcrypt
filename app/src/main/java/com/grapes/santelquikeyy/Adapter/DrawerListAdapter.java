package com.grapes.santelquikeyy.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.grapes.santelquikeyy.Model.NavigationItems;
import com.grapes.santelquikeyy.R;

import java.util.ArrayList;

/**
 * Created by prats on 5/25/2015.
 */
public class DrawerListAdapter extends BaseAdapter {

    Context mContext;
    ArrayList<NavigationItems> mNavItems;

    public DrawerListAdapter(Context context, ArrayList<NavigationItems> navItems) {
        mContext = context;
        mNavItems = navItems;
    }

    @Override
    public int getCount() {
        return mNavItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mNavItems.get(position);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.fragment_navigation_drawer, null);
        } else {

            view = convertView;
        }
        TextView titleView = (TextView) view.findViewById(R.id.title);
        ImageView img_drawer_menu=(ImageView)view.findViewById(R.id.img_drawer_menu);
        img_drawer_menu.setBackgroundResource(mNavItems.get(position).getImage());
        titleView.setText(mNavItems.get(position).getmTitle());
        return view;
    }
}
