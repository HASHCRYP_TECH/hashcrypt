package com.grapes.santelquikeyy.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import com.grapes.santelquikeyy.Activities.AddNewAddressFinalActivity;
import com.grapes.santelquikeyy.Model.TempAddsres;
import com.grapes.santelquikeyy.R;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

/**
 * Created by pc on 8/9/2017.
 */

public class AddressListAdapter extends BaseAdapter {
    Context context;
    ArrayList<TempAddsres>addsres;
    private static LayoutInflater inflater=null;
    private int postion_check=0;
    public AddressListAdapter(Context context, ArrayList<TempAddsres> addsresArrayList) {
        // TODO Auto-generated constructor stub
        this.addsres=addsresArrayList;
        this.context=context;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return addsres.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        TextView txt_nick;
        RadioButton rd_button;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final Holder holder=new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.address_list_layout, null);
        holder.txt_nick=(TextView) rowView.findViewById(R.id.txt_nick);
        holder.rd_button=(RadioButton)rowView.findViewById(R.id.rd_button);
        holder.rd_button.setText(addsres.get(position).getStr_address());
        String str_nick_name=addsres.get(position).getStr_nick_name();
        if (str_nick_name.equals(""))
        {

            holder.txt_nick.setVisibility(View.INVISIBLE);
        }else {
            holder.txt_nick.setVisibility(View.VISIBLE);
            holder.txt_nick.setText(str_nick_name);
        }
        if (postion_check==position)
        {
            holder.rd_button.setChecked(true);
        }
        holder.rd_button.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                holder.rd_button.setChecked(true);
                AddNewAddressFinalActivity.str_address=addsres.get(position).getStr_address();
                AddNewAddressFinalActivity.str_nick_name=addsres.get(position).getStr_nick_name();
                Intent intent=new Intent();
                intent.putExtra("str_name","");
                ((Activity)context).setResult(RESULT_OK,intent);
                ((Activity)context).finish();
                return false;
            }
        });
        {

//                if (isChecked)
//                {


//                }else {
//                    holder.rd_button.setChecked(false);
//                    AddNewAddressFinalActivity.str_address=addsres.get(position).getStr_address();
//                    AddNewAddressFinalActivity.str_nick_name=addsres.get(position).getStr_nick_name();
//                    Intent intent=new Intent();
//                    intent.putExtra("str_name","");
//                    ((Activity)context).setResult(RESULT_OK,intent);
//                    ((Activity)context).finish();
//                }

        }
        return rowView;
    }
}
