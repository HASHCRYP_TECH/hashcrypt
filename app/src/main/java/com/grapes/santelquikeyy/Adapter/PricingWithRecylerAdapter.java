package com.grapes.santelquikeyy.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.grapes.santelquikeyy.Model.Offer;
import com.grapes.santelquikeyy.Model.PricingModel;
import com.grapes.santelquikeyy.R;

import java.util.List;

/**
 * Created by pc on 7/21/2017.
 */

public class PricingWithRecylerAdapter extends RecyclerView.Adapter<PricingWithRecylerAdapter.MyViewHolder> {

    private List<PricingModel> list_offer_image;
    private Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView img_offer;
        public TextView  txt_title,txt_count_service,txt_count_price;;

        public MyViewHolder(View view) {
            super(view);
            txt_count_price=(TextView)view.findViewById(R.id.txt_count_price);
            txt_count_service=(TextView)view.findViewById(R.id.txt_count_service);
            txt_title=(TextView) view.findViewById(R.id.txt_title);
        }
    }
    public PricingWithRecylerAdapter(Context context, List<PricingModel> list_offer_image)
    {
        this.list_offer_image = list_offer_image;
        this.mContext=context;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pricing_text_list_item_layout, parent, false);
        return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.txt_title.setText(list_offer_image.get(position).getStr_pricing_name());
        holder.txt_count_service.setText(list_offer_image.get(position).getStr_pricing_service());
        holder.txt_count_price.setText(list_offer_image.get(position).getStr_pricing_price());
    }
    @Override
    public int getItemCount()
    {
        return list_offer_image.size();
    }
}
