package com.grapes.santelquikeyy.Activities;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.crash.FirebaseCrash;
import com.grapes.santelquikeyy.AActivities.AcServiceActivity;
import com.grapes.santelquikeyy.Adapter.DrawerListAdapter;
import com.grapes.santelquikeyy.Fragment.HomeFragment;
import com.grapes.santelquikeyy.Model.NavigationItems;
import com.grapes.santelquikeyy.R;
import com.grapes.santelquikeyy.UtilsApi;

import java.util.ArrayList;

/**
 * Created by pc on 7/24/2017.
 */

public class MainWithDrawerActivity extends AppCompatActivity {
    private ArrayList<NavigationItems> navigationItemsArrayList = new ArrayList<NavigationItems>();
    private DrawerLayout drawerLayout;
    private RelativeLayout drawerPanel;
    private ListView navigationList;
    private ActionBarDrawerToggle mDrawerToggle;
    private FragmentTransaction transaction;
    private String mActivityTitle;
    private TextView txt_profile_name;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_with_drawer_layout);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }
        // FirebaseApp.initializeApp(MainWithDrawerActivity.this);
        FirebaseCrash.report(new Exception("My first Android non-fatal error"));
        FirebaseCrash.log("Activity created");
        navigationItemsArrayList.add(new NavigationItems(R.drawable.home_menu, "Home "));
        navigationItemsArrayList.add(new NavigationItems(R.drawable.product_menu, "Products"));
        navigationItemsArrayList.add(new NavigationItems(R.drawable.service_menu, "Services"));
        navigationItemsArrayList.add(new NavigationItems(R.drawable.order_menu, "My Orders"));
        navigationItemsArrayList.add(new NavigationItems(R.drawable.account_menu, "Account"));
        navigationItemsArrayList.add(new NavigationItems(R.drawable.about_us_menu, "About us"));
        navigationItemsArrayList.add(new NavigationItems(R.drawable.offers_menu, "Offers"));
        navigationItemsArrayList.add(new NavigationItems(R.drawable.chat_menu, "Sales Chat"));
        navigationItemsArrayList.add(new NavigationItems(R.drawable.contacts_menu, "Contact Us"));
        navigationItemsArrayList.add(new NavigationItems(R.drawable.log_out_menu, "Logout"));
        // setTitle("");
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeButtonEnabled(true);
        mActivityTitle = getTitle().toString();
        // DrawerLayout
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        // Populate the Navigtion Drawer with options
        drawerPanel = (RelativeLayout) findViewById(R.id.drawerPanel);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        navigationList = (ListView) findViewById(R.id.navigationList);
        DrawerListAdapter adapter = new DrawerListAdapter(this, navigationItemsArrayList);
        navigationList.setAdapter(adapter);

        HomeFragment fragment = new HomeFragment();
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.mainContent, fragment).commit();

        txt_profile_name = (TextView) findViewById(R.id.txt_profile_name);
        SharedPreferences prefs = getSharedPreferences(UtilsApi.MY_PREFS_NAME, MODE_PRIVATE);
        String idregist = prefs.getString("str_username", null);
        SharedPreferences prefsLOGIN = getSharedPreferences(UtilsApi.MY_PREFS_NAME_LOGIN, MODE_PRIVATE);
        String idregistLOGIN = prefsLOGIN.getString("uname", null);
        SharedPreferences prefsFB = getSharedPreferences(UtilsApi.MY_PREFS_NAME_FB, MODE_PRIVATE);
        String idregistFb = prefsFB.getString("uname", null);
        if (idregist != null) {
            txt_profile_name.setText(idregist);
        } else if (idregistLOGIN != null) {
            txt_profile_name.setText(idregistLOGIN);
        } else if (idregistFb != null) {
            txt_profile_name.setText(idregistFb);
        }
        // Drawer Item click listeners
        navigationList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id
            ) {
                onSectionAttached(position);
            }
        });
        navigationList.setItemChecked(0, true);
        mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mActivityTitle);
                Toast.makeText(MainWithDrawerActivity.this, "Close", Toast.LENGTH_SHORT).show();
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle("Navigation!");
                Toast.makeText(MainWithDrawerActivity.this, "Open", Toast.LENGTH_SHORT).show();
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()            }
            }
        };
        mDrawerToggle.syncState();
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeButtonEnabled(true);
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 0:
                Toast.makeText(this, "Home", Toast.LENGTH_SHORT).show();

//                HomeFragment homeFragment = new HomeFragment();
//                transaction = getFragmentManager().beginTransaction();
//                transaction.replace(R.id.mainContent, homeFragment).commit();
                break;
            case 1:
              //  Toast.makeText(this, "Log In", Toast.LENGTH_SHORT).show();

//                LoginFragment loginFragment = new LoginFragment();
//                transaction = getFragmentManager().beginTransaction();
//                transaction.replace(R.id.mainContent, loginFragment).commit();
                startActivityForResult(new Intent(getApplicationContext(), AcServiceActivity.class), UtilsApi.REQUESTCODE);
                break;
            case 2:
                Intent intentser = new Intent(getApplicationContext(), NewACServicesRepairActivity.class);
//                Intent intentser = new Intent(getApplicationContext(), AcServiceActivity.class);
                startActivityForResult(intentser, UtilsApi.REQUESTCODE);
                //finish();

//                ProfileFragment profileFragment = new ProfileFragment();
//                FragmentTransaction transaction = getFragmentManager().beginTransaction();
//                transaction.replace(R.id.mainContent, profileFragment).commit();
                break;
            case 3:
                break;
            case 4:
                break;
            case 5:
                break;
            case 6:
                break;
            case 9:
                SharedPreferences myPrefsregi = getApplicationContext().getSharedPreferences(UtilsApi.MY_PREFS_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = myPrefsregi.edit();
                editor.clear();
                editor.commit();

                SharedPreferences myPrefsLogin = getApplicationContext().getSharedPreferences(UtilsApi.MY_PREFS_NAME_LOGIN, Context.MODE_PRIVATE);
                SharedPreferences.Editor editorlogin = myPrefsLogin.edit();
                editorlogin.clear();
                editorlogin.commit();


                SharedPreferences myPrefsfb = getApplicationContext().getSharedPreferences(UtilsApi.MY_PREFS_NAME_FB, Context.MODE_PRIVATE);
                SharedPreferences.Editor editorfb = myPrefsfb.edit();
                editorfb.clear();
                editorfb.commit();


                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivityForResult(intent, UtilsApi.REQUESTCODE);
                finish();

                break;
        }
        drawerLayout.closeDrawer(drawerPanel);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        int id = item.getItemId();

//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }


}
