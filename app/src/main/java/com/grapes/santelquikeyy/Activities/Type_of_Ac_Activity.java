package com.grapes.santelquikeyy.Activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.grapes.santelquikeyy.Adapter.TypeAdapter;
import com.grapes.santelquikeyy.Adapter.TypeRecAdapter;
import com.grapes.santelquikeyy.Model.MahiServiceClass;
import com.grapes.santelquikeyy.Model.TypeService;
import com.grapes.santelquikeyy.R;
import com.grapes.santelquikeyy.UtilsApi;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by pc on 7/31/2017.
 */

public class Type_of_Ac_Activity extends AppCompatActivity implements View.OnClickListener {
    TextView txt_what_type, txt_option_two, txt_option_one, txt_done, txt_complete;
    CheckBox chk_window_ac, chk_splite_ac;
    ArrayList<TypeService> typeServiceArrayList = new ArrayList<>();
    TextView txt_back, txt_cancel;
    ProgressBar progress_bar;
    public static RelativeLayout relative_done;
    RecyclerView recyler_type_service;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.type_of_unit_ac_1_layout);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.guide_text_color));
        }
        initViews();
    }

    private void initViews() {
        recyler_type_service = (RecyclerView) findViewById(R.id.recyler_type_service);
        relative_done = (RelativeLayout) findViewById(R.id.relative_done);
        relative_done.setOnClickListener(this);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
        txt_cancel = (TextView) findViewById(R.id.txt_cancel);
        txt_cancel.setOnClickListener(this);
        txt_back = (TextView) findViewById(R.id.txt_back);
        txt_back.setOnClickListener(this);
        txt_complete = (TextView) findViewById(R.id.txt_complete);
        txt_what_type = (TextView) findViewById(R.id.txt_what_type);
//        txt_option_one=(TextView)findViewById(R.id.txt_option_one);
//        txt_option_two=(TextView)findViewById(R.id.txt_option_two);
        txt_done = (TextView) findViewById(R.id.txt_done);

        txt_done.setOnClickListener(this);
        Load_Type_Services();
    }

    public void Load_Type_Services() {
//        final KProgressHUD hud = KProgressHUD.create(Type_of_Ac_Activity.this)
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                .setLabel("Please wait")
//                .setMaxProgress(100)
//                .show();
        String uri = UtilsApi.Services_Type_Url;
        StringRequest request = new StringRequest(uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                if (hud != null) {
//                    hud.dismiss();
//                }
                Log.d("Tag", response);

                String code = null;
                String message = null;
                String complete = null;
                try {
                    JSONArray array = new JSONArray(response);
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsonObject = array.getJSONObject(i);
                        code = jsonObject.getString("code");
                        message = jsonObject.getString("message");
                        complete = jsonObject.getString("complete");
                        if (code.equals("0")) {
                            txt_complete.setText(complete + " " + "Complete");
                            txt_what_type.setText(message);
                            txt_complete.setText(complete + " " + "Complete");
                            String result = complete.substring(0, complete.length() - 1);
                            progress_bar.setProgress(Integer.parseInt(result));
                        }
                        if (code.equals("0")) {
                            JSONArray phoneArray = jsonObject.getJSONArray("result");
                            for (int j = 0; j < phoneArray.length(); j++) {
                                JSONObject jsonObject1 = phoneArray.getJSONObject(j);
                                TypeService typeService = new TypeService();
                                typeService.setId(jsonObject1.getString("id"));
                                typeService.setName(jsonObject1.getString("name"));
                                typeService.setNumberType(j);
                                typeServiceArrayList.add(typeService);
                            }
                        }
                        if (typeServiceArrayList.size() > 0) {

                            TypeRecAdapter typeAdapter = new TypeRecAdapter(getApplicationContext(), typeServiceArrayList);
                            recyler_type_service.setHasFixedSize(true);
                            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                            recyler_type_service.setLayoutManager(layoutManager);
                            recyler_type_service.setAdapter(typeAdapter);
//                            txt_option_one.setText(typeServiceArrayList.get(0).getName());
//                            if (typeServiceArrayList.size()>1)
//                            {
//                                txt_option_two.setText(typeServiceArrayList.get(1).getName());
//                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (code.equals("0")) {

//                    Intent intent = new Intent(getApplicationContext(), MainWithDrawerActivity.class);
//                    startActivity(intent);
                } else if (code.equals("-1")) {
                    Toast.makeText(Type_of_Ac_Activity.this, "" + message, Toast.LENGTH_SHORT).show();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        if (hud != null) {
//                            hud.dismiss();
//                        }
                        Toast.makeText(Type_of_Ac_Activity.this, "Internet connection error", Toast.LENGTH_SHORT).show();
                    }
                });
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.relative_done:
                get_type_of_servies();

                break;
            case R.id.txt_back:
                onBackPressed();
                break;
            case R.id.txt_cancel:
                Intent intent1 = new Intent(getApplicationContext(), MainWithDrawerActivity.class);
                startActivityForResult(intent1, UtilsApi.REQUESTCODE);
                //finish();
                break;
        }
    }

    private void get_type_of_servies() {
        StringBuilder stringBuilder = new StringBuilder();
        String str_w_id = "", str_s_id;
        for (TypeService appList : this.typeServiceArrayList) {
            if (appList.isSelected()) {
                // appList.setSelected(false);
                if (stringBuilder.length() > 0)
                    stringBuilder.append(", ");
                stringBuilder.append(appList.getNumberType());
                int position = appList.getNumberType();
                str_w_id = this.typeServiceArrayList.get(position).getId();
                str_s_id = this.typeServiceArrayList.get(position).getId();
                UtilsApi.arrayList_id.add(str_w_id);
            }
        }

        ArrayList<MahiServiceClass> mahiServiceClassArrayList = new ArrayList<>();
        for (int i = 0; i < typeServiceArrayList.size(); i++) {
            MahiServiceClass mahiServiceClass = new MahiServiceClass();
            mahiServiceClass.setTypeOfAc(this.typeServiceArrayList.get(i).getId());
            mahiServiceClassArrayList.add(mahiServiceClass);
        }
        if (UtilsApi.arrayList_id.size() > 0) {
            Intent intent = new Intent(getApplicationContext(), TypeAcSubActivity.class);
            intent.putExtra("str_w_id", str_w_id);
            intent.putExtra("arrayForAcType", mahiServiceClassArrayList);
            startActivityForResult(intent, UtilsApi.REQUESTCODE);
            // finish();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent intent=new Intent(getApplicationContext(),NewACServicesRepairActivity.class);
//        startActivityForResult(intent,UtilsApi.REQUESTCODE);
        finish();
    }
}
