package com.grapes.santelquikeyy.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.grapes.santelquikeyy.R;
import com.grapes.santelquikeyy.UtilsApi;

/**
 * Created by pc on 7/19/2017.
 */

public class SplashScreenActivity extends AppCompatActivity
{
    private static int SPLASH_TIME_OUT = 3000;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen_layout);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                SharedPreferences prefs = getSharedPreferences(UtilsApi.MY_PREFS_NAME, MODE_PRIVATE);
                String idregist = prefs.getString("str_emailid", null);
                SharedPreferences prefsLOGIN = getSharedPreferences(UtilsApi.MY_PREFS_NAME_LOGIN, MODE_PRIVATE);
                String idregistLOGIN = prefsLOGIN.getString("emailid", null);
                SharedPreferences prefsFB = getSharedPreferences(UtilsApi.MY_PREFS_NAME_FB, MODE_PRIVATE);
                String idregistFb = prefsFB.getString("str_fbemailid", null);
                if (idregist!=null) {
                    Intent i = new Intent(SplashScreenActivity.this, MainWithDrawerActivity.class);
                    startActivity(i);
                    finish();
                }else if (idregistLOGIN!=null)
                {
                    Intent i = new Intent(SplashScreenActivity.this, MainWithDrawerActivity.class);
                    startActivity(i);
                    finish();
                }else if (idregistFb!=null)
                {
                    Intent i = new Intent(SplashScreenActivity.this, MainWithDrawerActivity.class);
                    startActivity(i);
                    finish();
                }
                else {
                    Intent i = new Intent(SplashScreenActivity.this, GuideActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        }, SPLASH_TIME_OUT);
    }

}
