package com.grapes.santelquikeyy.Activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.grapes.santelquikeyy.Adapter.TypeOfPriceAdapter;
import com.grapes.santelquikeyy.Adapter.TypeRecAdapter;
import com.grapes.santelquikeyy.Adapter.TypeRecSubAdapter;
import com.grapes.santelquikeyy.Adapter.TypeSubAdapter;
import com.grapes.santelquikeyy.Model.ArrayListTempAcSub;
import com.grapes.santelquikeyy.Model.MahiServiceClass;
import com.grapes.santelquikeyy.Model.PriceTextServiceModel;
import com.grapes.santelquikeyy.Model.TypeOfServicesTwo;
import com.grapes.santelquikeyy.Model.TypeService;
import com.grapes.santelquikeyy.R;
import com.grapes.santelquikeyy.UtilsApi;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pc on 7/31/2017.
 */

public class TypeAcSubActivity extends AppCompatActivity implements View.OnClickListener {
    TextView txt_complete, txt_what_type, txt_done;
    // TextView txt_option_one,txt_option_two,txt_option_three,txt_option_four,txt_done;
    ProgressBar progress_bar;
    //ListView list_item;
    //  CheckBox checkbox_1,checkbox_2,checkbox_3,checkbox_4;
    ArrayList<TypeOfServicesTwo> typeOfServicesTwos = new ArrayList<>();
    TextView txt_back, txt_cancel;
    public static RelativeLayout relative_done;
    String str_w_id;
    RecyclerView recyler_type_service;

    String str_window;

    ArrayList<MahiServiceClass> mahiServiceClassArrayList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.guide_text_color));
        }
        setContentView(R.layout.type_of_unit_ac_2_layout);
//        initViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initViews();
    }

    private void initViews() {
        if (UtilsApi.arrayList_id.size() > 0) {
            str_window = UtilsApi.arrayList_id.get(0);
        }
        str_w_id = getIntent().getStringExtra("str_w_id");

        try {
            mahiServiceClassArrayList = (ArrayList<MahiServiceClass>) getIntent().getSerializableExtra("arrayForAcType");
        } catch (Exception e) {
            e.printStackTrace();
        }

        relative_done = (RelativeLayout) findViewById(R.id.relative_done);
        relative_done.setOnClickListener(this);
        txt_cancel = (TextView) findViewById(R.id.txt_cancel);
        txt_cancel.setOnClickListener(this);
        txt_back = (TextView) findViewById(R.id.txt_back);
        txt_back.setOnClickListener(this);
        recyler_type_service = (RecyclerView) findViewById(R.id.recyler_type_service);
        txt_done = (TextView) findViewById(R.id.txt_done);
        txt_done.setOnClickListener(this);
        txt_complete = (TextView) findViewById(R.id.txt_complete);
        txt_what_type = (TextView) findViewById(R.id.txt_what_type);
//        txt_option_one=(TextView)findViewById(R.id.txt_option_one);
//        txt_option_two=(TextView)findViewById(R.id.txt_option_two);
//        txt_option_three=(TextView)findViewById(R.id.txt_option_three);
//        txt_option_four=(TextView)findViewById(R.id.txt_option_four);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
//        checkbox_1=(CheckBox)findViewById(R.id.checkbox_1);
//        checkbox_2=(CheckBox)findViewById(R.id.checkbox_2);
//        checkbox_3=(CheckBox)findViewById(R.id.checkbox_3);
//        checkbox_4=(CheckBox)findViewById(R.id.checkbox_4);

        Load_Type_Services();
    }

    /*@Override
    protected void onResume() {
        super.onResume();
        if (NewACServicesRepairActivity.arrayListTypeOfTwo.size() > 0) {
            txt_what_type.setText(NewACServicesRepairActivity.arrayListTypeOfTwo.get(0).getTitle());
            recyler_type_service.setHasFixedSize(true);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
            recyler_type_service.setLayoutManager(layoutManager);
            TypeRecSubAdapter typeOfPriceAdapter = new TypeRecSubAdapter(TypeAcSubActivity.this, NewACServicesRepairActivity.arrayListTypeOfTwo);
            recyler_type_service.setAdapter(typeOfPriceAdapter);
        }
    }*/

    public void Load_Type_Services() {
        typeOfServicesTwos = new ArrayList<>();
        NewACServicesRepairActivity.arrayList_service_check = new ArrayList<>();
        NewACServicesRepairActivity.arrayList_count = new ArrayList<>();
        NewACServicesRepairActivity.arrayList_show_display_service_name_1 = new ArrayList<>();
//        final KProgressHUD hud = KProgressHUD.create(TypeAcSubActivity.this)
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                .setLabel("Please wait")
//                .setMaxProgress(100)
//                .show();
        String uri = UtilsApi.Services_Sub_Type_Url + "services_type.php?id=" + str_window;
        StringRequest request = new StringRequest(uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                if (hud != null) {
//                    hud.dismiss();
//                }
                Log.d("Tag", response);

                String code = null;
                String message = null;
                //String complete=null;
                String complete;
                try {
                    JSONArray array = new JSONArray(response);
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsonObject = array.getJSONObject(i);
                        code = jsonObject.getString("code");
                        //message=jsonObject.getString("message");
                        complete = jsonObject.getString("complete");
                        if (code.equals("0")) {
                            txt_complete.setText(complete + " " + "Complete");
                            String result = complete.substring(0, complete.length() - 1);
                            progress_bar.setProgress(Integer.parseInt(result));
                            txt_what_type.setText(message);
                        }
                        if (code.equals("0")) {
                            JSONArray phoneArray = jsonObject.getJSONArray("result");
                            for (int j = 0; j < phoneArray.length(); j++) {
                                JSONObject jsonObject1 = phoneArray.getJSONObject(j);
                                TypeOfServicesTwo typeService = new TypeOfServicesTwo();
                                typeService.setId(jsonObject1.getString("id"));
                                typeService.setTitle(jsonObject1.getString("title"));
                                typeService.setName(jsonObject1.getString("name"));
                                typeService.setSelected(false);
                                typeService.setNumberType(j);
                                typeOfServicesTwos.add(typeService);
                            }
                        }
                        if (typeOfServicesTwos.size() > 0) {
//                            NewACServicesRepairActivity.arrayListTypeOfTwo = typeOfServicesTwos;
                            txt_what_type.setText(typeOfServicesTwos.get(0).getTitle());
                            recyler_type_service.setHasFixedSize(true);
                            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                            recyler_type_service.setLayoutManager(layoutManager);
                            TypeRecSubAdapter typeOfPriceAdapter = new TypeRecSubAdapter(TypeAcSubActivity.this, typeOfServicesTwos);
                            recyler_type_service.setAdapter(typeOfPriceAdapter);
//                            txt_option_one.setText(typeOfServicesTwos.get(0).getName());
//                            if (typeOfServicesTwos.size()>1)
//                            {
//                                txt_option_two.setText(typeOfServicesTwos.get(1).getName());
//                            }
//                            if (typeOfServicesTwos.size()>2)
//                            {
//                                txt_option_three.setText(typeOfServicesTwos.get(2).getName());
//                            }
//                            if (typeOfServicesTwos.size()>3)
//                            {
//                                txt_option_four.setText(typeOfServicesTwos.get(3).getName());
//                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (code.equals("0")) {

//                    Intent intent = new Intent(getApplicationContext(), MainWithDrawerActivity.class);
//                    startActivity(intent);
                } else if (code.equals("-1")) {
                    Toast.makeText(TypeAcSubActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        if (hud != null) {
//                            hud.dismiss();
//                        }
                        Toast.makeText(TypeAcSubActivity.this, "Internet connection error", Toast.LENGTH_SHORT).show();
                    }
                });
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.relative_done:
                get_type_of_servies();
//                Intent intent=new Intent(getApplicationContext(),TypeOfPriceActivity.class);
//                startActivityForResult(intent,UtilsApi.REQUESTCODE);
//                finish();
                break;
            case R.id.txt_back:
                onBackPressed();
                break;
            case R.id.txt_cancel:
                Intent intent1 = new Intent(getApplicationContext(), MainWithDrawerActivity.class);
                intent1.putExtra("str_w_id", str_w_id);
                startActivityForResult(intent1, UtilsApi.REQUESTCODE);
                // finish();
                break;
        }
    }

    private void get_type_of_servies() {
        StringBuilder stringBuilder = new StringBuilder();
        String str_w_id = "", str_s_id;
        if (NewACServicesRepairActivity.arrayList_show_display_service_name_1.size() > 0) {
            NewACServicesRepairActivity.arrayList_show_display_service_name_1.clear();
        }
        for (TypeOfServicesTwo appList : this.typeOfServicesTwos) {
            if (appList.isSelected()) {
                //  appList.setSelected(false);
                if (stringBuilder.length() > 0)
                    stringBuilder.append(", ");
                stringBuilder.append(appList.getNumberType());
                int position = appList.getNumberType();
                str_w_id = this.typeOfServicesTwos.get(position).getId();
                str_s_id = this.typeOfServicesTwos.get(position).getId();
                UtilsApi.arrayList_check_number_of_value.add(str_s_id);

                PriceTextServiceModel priceTextServiceModel = new PriceTextServiceModel();
                priceTextServiceModel.setStr_sevice_name(typeOfServicesTwos.get(position).getName());
//                priceTextServiceModel.setStr_price(typeOfServicesTwos.get(position).getName());
                NewACServicesRepairActivity.arrayList_show_display_service_name_1.add(priceTextServiceModel);
            }
        }
        /*for (TypeOfServicesTwo appList : typeOfServicesTwos) {
            if (appList.isSelected()) {
                //  appList.setSelected(false);
                if (stringBuilder.length() > 0)
                    stringBuilder.append(", ");
                stringBuilder.append(appList.getNumberType());
                int position = appList.getNumberType();
                str_w_id = NewACServicesRepairActivity.arrayListTypeOfTwo.get(position).getId();
                str_s_id = NewACServicesRepairActivity.arrayListTypeOfTwo.get(position).getId();
                UtilsApi.arrayList_check_number_of_value.add(str_s_id);

                PriceTextServiceModel priceTextServiceModel = new PriceTextServiceModel();
                priceTextServiceModel.setStr_sevice_name(NewACServicesRepairActivity.arrayListTypeOfTwo.get(position).getName());
//                priceTextServiceModel.setStr_price(typeOfServicesTwos.get(position).getName());
                NewACServicesRepairActivity.arrayList_show_display_service_name_1.add(priceTextServiceModel);
            }
        }*/
        if (UtilsApi.arrayList_id.size() > 0) {
            //UtilsApi.arrayList_id.remove(0);
        }

        /*ArrayList<MahiServiceClass> mahiServiceClassArrayList = new ArrayList<>();
        for (int i = 0; i < typeServiceArrayList.size(); i++) {
            MahiServiceClass mahiServiceClass = new MahiServiceClass();
            mahiServiceClass.setTypeOfAc(this.typeServiceArrayList.get(i).getId());
            mahiServiceClassArrayList.add(mahiServiceClass);
        }
*/
        if (!str_w_id.isEmpty()) {
            Intent intent = new Intent(getApplicationContext(), TypeOfPriceActivity.class);
            intent.putExtra("str_window", str_window);
            startActivityForResult(intent, UtilsApi.REQUESTCODE);
            //finish();
        }
    }

    private ArrayList<Integer> arrayList = new ArrayList<>();
    public ArrayList<String> arrayListserviceCheck = new ArrayList<>();
    public ArrayList<String> arrayList_count = new ArrayList<>();
    public ArrayList<ArrayListTempAcSub> arrayList_temp = new ArrayList<>();


    public void addremovesubtype(boolean isChecked, int i) {
        if (isChecked) {
            arrayList.add(1);
            PriceTextServiceModel priceTextServiceModel = new PriceTextServiceModel();
            priceTextServiceModel.setStr_sevice_name(typeOfServicesTwos.get(i).getName());
            arrayListserviceCheck.add(typeOfServicesTwos.get(i).getName());
            String str_count = String.valueOf(i + 1);
            ArrayListTempAcSub arrayListTempAcSub = new ArrayListTempAcSub(i, str_count);
            arrayList_temp.add(arrayListTempAcSub);
            arrayList_count.add(str_count);
            // NewACServicesRepairActivity.arrayList_show_display_service_name_1.add(priceTextServiceModel);
            TypeAcSubActivity.relative_done.setBackgroundResource(R.drawable.rounded_textview_button_black);
        } else {
            if (NewACServicesRepairActivity.arrayList_show_display_service_name_1.size() > 0) {
                String str_remove = typeOfServicesTwos.get(i).getName();
                // NewACServicesRepairActivity.arrayList_show_display_service_name_1.remove(i);
                arrayListserviceCheck.remove(str_remove);

                String str_count = "0";
                for (int j = 0; j < arrayList_temp.size(); j++) {
                    if (i == arrayList_temp.get(j).getPosition()) {
                        str_count = arrayList_temp.get(i).getStrintCount();
                    }
                }

                arrayList_count.remove(str_count);
                if (arrayListserviceCheck.size() > 0) {
                    //NewACServicesRepairActivity.arrayList_service_check.remove(i);
                }
                if (arrayList_count.size() > 0) {

                    //NewACServicesRepairActivity.arrayList_count.remove(i);
                }
            }
            if (arrayList.size() > 0) {
                arrayList.remove(0);
            }
            if (arrayList.size() > 0) {

            } else {
                TypeAcSubActivity.relative_done.setBackgroundResource(R.drawable.rounded_textview_button_gray);
            }

//            recyler_type_service.setAdapter();
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent intent=new Intent(getApplicationContext(),Type_of_Ac_Activity.class);
//        startActivityForResult(intent,UtilsApi.REQUESTCODE);
        finish();
    }
}
