package com.grapes.santelquikeyy.Activities;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.grapes.santelquikeyy.Adapter.PricingAdapter;
import com.grapes.santelquikeyy.Adapter.PricingWithRecylerAdapter;
import com.grapes.santelquikeyy.Model.PriceTextServiceModel;
import com.grapes.santelquikeyy.Model.PricingModel;
import com.grapes.santelquikeyy.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pc on 8/11/2017.
 */

public class PricingDetailsActivity extends AppCompatActivity implements View.OnClickListener {
    RelativeLayout relative_done;
    TextView txt_total_price, txt_total_service, txt_count_price, txt_count_service, txt_back;
    RecyclerView list_view;
    List<PricingModel> arrayList = new ArrayList<>();

    String substr;
    ArrayList<Integer> arrayList_integr = new ArrayList<>();
    ArrayList<Integer> arr_value = new ArrayList<>();
    ArrayList<PriceTextServiceModel> arr_table_show = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_pricing_details_layout);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }

        initView();
    }

    private void initView() {

        list_view = (RecyclerView) findViewById(R.id.list_view);
        try {
            for (int i = 0; i < NewACServicesRepairActivity.arrayList_show_display_service_name_1.size(); i++) {
                String str_service = NewACServicesRepairActivity.arrayList_show_display_service_name_1.get(i).getStr_sevice_name();
                String str_price = NewACServicesRepairActivity.arrayList_show_display_service_name_1.get(i).getStr_price();
                //String str_price=NewACServicesRepairActivity.arrayList_show_display_service_price_2.get(i);
                substr = str_price.substring(str_price.indexOf(str_price)/* + 5*/);
                PricingModel pricingModel = new PricingModel();
                pricingModel.setStr_pricing_name(str_service);
                pricingModel.setStr_pricing_price(substr);

                String[] parts = substr.split("\\.");
                String strPricing = substr.replaceAll(".*.", "");
                Log.e("strpricing", strPricing + "  ; part on1 : " + parts[1]);
                int temp = Integer.parseInt(parts[1]);
                NewACServicesRepairActivity.finaltotal = NewACServicesRepairActivity.totalTemp + temp;
                NewACServicesRepairActivity.totalTemp = NewACServicesRepairActivity.finaltotal;

                pricingModel.setStr_pricing_service("1");
                arrayList.add(pricingModel);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (arrayList.size() > 0) {
            PricingWithRecylerAdapter adapter = new PricingWithRecylerAdapter(PricingDetailsActivity.this, arrayList);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            list_view.setLayoutManager(mLayoutManager);
            list_view.setAdapter(adapter);
        }
//        relative_done=(RelativeLayout)findViewById(R.id.relative_done);
//        relative_done.setOnClickListener(this);
//        txt_count_price=(TextView)findViewById(R.id.txt_count_price);
//        txt_total_price=(TextView)findViewById(R.id.txt_total_price);
//        txt_total_service=(TextView)findViewById(R.id.txt_total_service);
//        txt_count_service=(TextView)findViewById(R.id.txt_count_service);
        txt_total_price = (TextView) findViewById(R.id.txt_total_price);
        int size = NewACServicesRepairActivity.arrayList_show_display_service_price_2.size();

        String styr = String.valueOf(NewACServicesRepairActivity.finaltotal);
        txt_total_price.setText(styr);
        NewACServicesRepairActivity.finaltotal = 0;
        NewACServicesRepairActivity.totalTemp = 0;
        txt_back = (TextView) findViewById(R.id.txt_back);
        txt_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.relative_done:
                Toast.makeText(this, "Submited..", Toast.LENGTH_SHORT).show();
                break;
            case R.id.txt_back:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
