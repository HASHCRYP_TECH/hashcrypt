package com.grapes.santelquikeyy.Activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.grapes.santelquikeyy.R;

/**
 * Created by pc on 8/8/2017.
 */

public class Add_AddressActivity extends AppCompatActivity implements View.OnClickListener {
    RelativeLayout relative_schedule_s_service,relative_add_address;
    TextView txt_back,txt_change;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_address_layout);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }
        initView();
    }
    private void initView() {
        NewACServicesRepairActivity.check=true;
        txt_change=(TextView)findViewById(R.id.txt_change);
        txt_back=(TextView)findViewById(R.id.txt_back);
        relative_schedule_s_service=(RelativeLayout)findViewById(R.id.relative_schedule_s_service);
        relative_add_address=(RelativeLayout)findViewById(R.id.relative_add_address);

        txt_back.setOnClickListener(this);
        txt_change.setOnClickListener(this);
        relative_schedule_s_service.setOnClickListener(this);
        relative_add_address.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.txt_back:
                finish();
                break;
            case R.id.txt_change:
                break;
            case R.id.relative_schedule_s_service:
                break;
            case R.id.relative_add_address:
                Intent intent=new Intent(getApplicationContext(),Add_New_Activity.class);
                startActivityForResult(intent,1001);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
