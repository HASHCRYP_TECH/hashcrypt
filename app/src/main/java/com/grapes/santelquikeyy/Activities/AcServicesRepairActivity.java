package com.grapes.santelquikeyy.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;

import com.grapes.santelquikeyy.R;
import com.grapes.santelquikeyy.UtilsApi;

/**
 * Created by pc on 7/31/2017.
 */

public class AcServicesRepairActivity extends AppCompatActivity implements View.OnClickListener {
    RelativeLayout relative_schedule_s_service;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_services_and_repaire_and_layout);

        initViews();
    }

    private void initViews() {
        relative_schedule_s_service=(RelativeLayout)findViewById(R.id.relative_schedule_s_service);
        relative_schedule_s_service.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.relative_schedule_s_service:
                Intent intent=new Intent(getApplicationContext(),Type_of_Ac_Activity.class);
                startActivityForResult(intent, UtilsApi.REQUESTCODE);
                finish();
                break;
        }
    }
    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Intent intent=new Intent(getApplicationContext(),MainWithDrawerActivity.class);
        startActivityForResult(intent, UtilsApi.REQUESTCODE);
        finish();
    }
}
