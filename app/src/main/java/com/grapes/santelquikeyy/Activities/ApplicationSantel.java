package com.grapes.santelquikeyy.Activities;

import android.app.Application;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.firebase.FirebaseApp;

/**
 * Created by pc on 7/27/2017.
 */

public class ApplicationSantel extends Application
{

    public static final String TAG = ApplicationSantel.class.getSimpleName();

    private RequestQueue mRequestQueue;

    private static ApplicationSantel mInstance;
    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(this);

        mInstance = this;
    }
    public static synchronized ApplicationSantel getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}
