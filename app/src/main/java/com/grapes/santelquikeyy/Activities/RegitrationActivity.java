package com.grapes.santelquikeyy.Activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.grapes.santelquikeyy.R;
import com.grapes.santelquikeyy.UtilsApi;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pc on 7/19/2017.
 */

public class RegitrationActivity extends AppCompatActivity implements View.OnClickListener {
    EditText edt_name, edt_email, edt_mobile, edt_password;
    RelativeLayout sign_up;
    TextView txt_sing_up,txt_privacy_policy,txt_forgot_password;
    String str_username,str_emailid,str_id;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_layout);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        initViews();
    }

    private void initViews() {
        txt_forgot_password=(TextView)findViewById(R.id.txt_forgot_password);
        txt_forgot_password.setOnClickListener(this);
        txt_privacy_policy=(TextView)findViewById(R.id.txt_privacy_policy);
        txt_privacy_policy.setOnClickListener(this);
        txt_sing_up=(TextView)findViewById(R.id.txt_sing_up);
        edt_name = (EditText) findViewById(R.id.edt_name);
        edt_email = (EditText) findViewById(R.id.edt_email);
        edt_mobile = (EditText) findViewById(R.id.edt_mobile);
        edt_password = (EditText) findViewById(R.id.edt_password);
        sign_up = (RelativeLayout) findViewById(R.id.sign_up);
        sign_up.setOnClickListener(this);
        txt_sing_up.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_up:
                loadRegistration();
                break;
            case R.id.txt_sing_up:
                finish();
                break;
            case R.id.txt_privacy_policy:
                Intent intent1=new Intent(getApplicationContext(),PrivacyPolicyActivity.class);
                startActivityForResult(intent1,UtilsApi.REQUESTCODE);
                break;
            case R.id.txt_forgot_password:
                loadDialogForgotPassword();
                break;
        }
    }

    private void loadRegistration() {
        String str_name = edt_name.getText().toString().trim();
        String str_email = edt_email.getText().toString().trim();
        String str_mobile = edt_mobile.getText().toString().trim();
        String str_password = edt_password.getText().toString().trim();
        String emailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        if (!str_name.isEmpty()) {
            if (!str_email.isEmpty())
            {
                if (str_email.matches(emailPattern)) {
                    if (!str_mobile.isEmpty())
                    {
                        if (!str_password.isEmpty()) {
                            requestData(str_name, str_email, str_mobile, str_password);
                        } else {
                            Toast.makeText(this, "Enter password..", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(this, "Enter mobile..", Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Toast.makeText(getApplicationContext(), "Invalid email address", Toast.LENGTH_SHORT).show();
                }

            } else {
                Toast.makeText(this, "Enter email..", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Enter name..", Toast.LENGTH_SHORT).show();
        }
    }

    public void requestData(String str_name, String str_email, String str_mobile, String str_password) {
        final KProgressHUD hud = KProgressHUD.create(RegitrationActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setMaxProgress(100)
                .show();
        String uri = UtilsApi.REGISTRATION_URL + "registration.php?username=" + str_name + "&emailid=" + str_email + "&password=" + str_password + "&mobile="+str_mobile+"&photo=";
        StringRequest request = new StringRequest(uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (hud != null) {
                    hud.dismiss();
                }
                Log.d("Tag", response);

                String code = null;
                String message=null;
                try {
                    JSONArray array=new JSONArray(response);
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsonObject=array.getJSONObject(i);
                         code=jsonObject.getString("code");
                        message=jsonObject.getString("message");
                        if (code.equals("0")) {
                             str_username = jsonObject.getString("username");
                             str_emailid = jsonObject.getString("emailid");
                             str_id = jsonObject.getString("id");
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (code.equals("0"))
                {
                    Toast.makeText(RegitrationActivity.this, ""+message, Toast.LENGTH_SHORT).show();
                    SharedPreferences.Editor editor = getSharedPreferences(UtilsApi.MY_PREFS_NAME, MODE_PRIVATE).edit();
                    editor.putString("str_username",str_username);
                    editor.putString("str_id",str_id);
                    editor.putString("str_emailid",str_emailid);
                    editor.commit();
                    Intent intent = new Intent(getApplicationContext(), MainWithDrawerActivity.class);
                    startActivity(intent);
                }else if (code.equals("-1"))
                {
                    Toast.makeText(RegitrationActivity.this, ""+message, Toast.LENGTH_SHORT).show();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (hud != null) {
                            hud.dismiss();
                        }
                        Toast.makeText(RegitrationActivity.this, "Connnection Error", Toast.LENGTH_SHORT).show();
                    }
                });

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }
    private void loadDialogForgotPassword() {
        final Dialog dialog=new Dialog(RegitrationActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.forgot_popup_layout);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView txt_cancel=(TextView)dialog.findViewById(R.id.txt_cancel);
        TextView txt_send=(TextView)dialog.findViewById(R.id.txt_send);
        final EditText edt_email=(EditText)dialog.findViewById(R.id.edt_email);

        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        txt_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                String str_email=edt_email.getText().toString().trim();
                loadForgotPassword(str_email);
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    private void loadForgotPassword(String str_email)
    {
        final KProgressHUD hud = KProgressHUD.create(RegitrationActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setMaxProgress(100)
                .show();
        String uri = UtilsApi.BASE_URL + "forgot_password.php?emailid="+str_email;
        StringRequest request = new StringRequest(uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {
                if (hud != null) {
                    hud.dismiss();
                }
                Log.d("Tag", response);
                String message = null;
                String code = null;
                Log.d("Tag", response);
                try {
                    JSONArray array = new JSONArray(response);
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsonObject = array.getJSONObject(i);
                        message = jsonObject.getString("message");
                        code = jsonObject.getString("code");
                        Log.d("Tag", message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (hud != null) {
                            hud.dismiss();
                        }
                        Toast.makeText(RegitrationActivity.this, "Internet connection error", Toast.LENGTH_SHORT).show();
                    }
                });
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
