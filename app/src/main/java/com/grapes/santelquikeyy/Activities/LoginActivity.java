package com.grapes.santelquikeyy.Activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.grapes.santelquikeyy.R;
import com.grapes.santelquikeyy.UtilsApi;
import com.grapes.santelquikeyy.api.ApiClient;
import com.grapes.santelquikeyy.api.ApiInterface;
import com.grapes.santelquikeyy.responseModel.FacebookModel;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by pc on 7/19/2017.
 */

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    TextView btn_sing_in, txt_sing_up, txt_forgot_password, txt_privacy_policy;
    EditText edt_email, edt_password;
    RelativeLayout sign_up;
    RelativeLayout relative_fb_login;
    private CallbackManager callbackManager;
    LoginButton login_button;
    String uname, emailid, id;
    String str_username_fb, str_fbemailid, str_id_fb;
    private KProgressHUD hud;
    private RequestQueue requestQueue;
    ApiInterface apiService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.loging_layout);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        printHashKey();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        initView();
    }

    public void printHashKey() {
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.ac.santelquikeyy",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

    private void initView() {

        requestQueue = Volley.newRequestQueue(getApplicationContext());

        txt_forgot_password = (TextView) findViewById(R.id.txt_forgot_password);
        txt_privacy_policy = (TextView) findViewById(R.id.txt_privacy_policy);
        txt_privacy_policy.setOnClickListener(this);
        txt_forgot_password.setOnClickListener(this);
        login_button = (LoginButton) findViewById(R.id.login_button);
        login_button.setReadPermissions(Arrays.asList("public_profile", "email"));
        login_button.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                login_button.setVisibility(View.VISIBLE);

                GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.d("JSON", "" + response.getJSONObject().toString());
                        try {
                            String id = object.getString("id");
                            String imgUrl = "https://graph.facebook.com/" + id + "/picture?type=large";
                            try {
                                String emailid1 = object.getString("email");
                                emailid = emailid1;
                            } catch (Exception e) {
                                String strId = object.getString("id");
                                emailid = strId;
                                e.printStackTrace();
                            }
                            String username = object.getString("name");
                            String photo = "";
                            Log.d("Tag", id + "\n" + imgUrl);
                            FaceBookApi(id, emailid, username);

//                            // LoadFaceBook(id, emailid, username, imgUrl);
//                            makeJsonArrayRequest(id, emailid, username, imgUrl);
//                            // loadFaceBookNew(id,emailid,username,imgUrl);
//                            // fetch_FB(id,emailid,username,imgUrl);


                        } catch (JSONException e) {
                            LoginManager.getInstance().logOut();
                            Toast.makeText(LoginActivity.this, "Email not found..", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,first_name,last_name,email");
                graphRequest.setParameters(parameters);
                graphRequest.executeAsync();
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException exception) {
            }
        });
//        relative_fb_login=(RelativeLayout)findViewById(R.id.relative_fb_login);
//        relative_fb_login.setOnClickListener(this);
        txt_sing_up = (TextView) findViewById(R.id.txt_sing_up);
        txt_sing_up.setOnClickListener(this);
        edt_password = (EditText) findViewById(R.id.edt_password);
        edt_email = (EditText) findViewById(R.id.edt_email);
        final String email = edt_email.getText().toString().trim();
        final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
//        edt_email .addTextChangedListener(new TextWatcher() {
//            public void afterTextChanged(Editable s) {
//
//                if (email.matches(emailPattern) && s.length() > 0)
//                {
//                    Toast.makeText(getApplicationContext(),"valid email address",Toast.LENGTH_SHORT).show();
//                    // or
//                }
//                else
//                {
//                    Toast.makeText(getApplicationContext(),"Invalid email address",Toast.LENGTH_SHORT).show();
//                    //or
//                }
//            }
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                // other stuffs
//            }
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                // other stuffs
//            }
//        });
        sign_up = (RelativeLayout) findViewById(R.id.sign_up);
        sign_up.setOnClickListener(this);
    }

    private void FaceBookApi(String Id, String EmailId, String Username) {

        final KProgressHUD hud = KProgressHUD.create(LoginActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setMaxProgress(100)
                .show();

        Call<List<FacebookModel>> facebookModelCall = apiService.getFacebookResponse(Id, EmailId, Username);

        facebookModelCall.enqueue(new Callback<List<FacebookModel>>() {
            @Override
            public void onResponse(Call<List<FacebookModel>> call, retrofit2.Response<List<FacebookModel>> response) {

                if (response.isSuccessful()) {
                    FacebookModel facebookModel = response.body().get(0);
                    int code = facebookModel.getCode();
                    Log.d("tag", "code" + code);
                    if (code == 0) {
                        SharedPreferences.Editor editor = getSharedPreferences(UtilsApi.MY_PREFS_NAME_FB, MODE_PRIVATE).edit();
                        editor.putString("str_fbemailid", facebookModel.getEmailid());
                        editor.putString("id", facebookModel.getId());
                        editor.putString("uname", facebookModel.getUsername());
                        editor.commit();
                        Toast.makeText(LoginActivity.this, "" + facebookModel.getMessage(), Toast.LENGTH_SHORT).show();
                        LoginManager.getInstance().logOut();
                        Intent intent = new Intent(getApplicationContext(), MainWithDrawerActivity.class);
                        startActivity(intent);
                        finish();
                    } else if (code == -1) {
                        Toast.makeText(LoginActivity.this, "" + facebookModel.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void onFailure(Call<List<FacebookModel>> call, Throwable t) {
                Log.d("tag", "Error :" + t.toString());
            }
        });


    }

    private void fetch_FB(String id, String emailid, String username, String imgUrl) {
        hud = KProgressHUD.create(LoginActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setMaxProgress(100)
                .show();
        String uri = UtilsApi.FACEB0OK_URL + "facebook_login.php?fb_id=" + id + "&emailid=" + emailid + "&username=" + username + "&photo=" + imgUrl;
        StringRequest request = new StringRequest(Request.Method.POST, uri, onPostsLoaded, onPostsError);
        requestQueue.add(request);
    }

    private final Response.Listener<String> onPostsLoaded = new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
            Log.i("PostActivity", response);
            Log.d("TAG", response.toString());
            if (hud != null) {
                hud.dismiss();
            }
            Log.d("Tag", response.toString());
            String message = null;
            String code = null;
            Log.d("Tag", response.toString());
            try {
                JSONArray array = new JSONArray(response);
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsonObject = array.getJSONObject(i);
                    message = jsonObject.getString("message");
                    code = jsonObject.getString("code");
                    str_fbemailid = jsonObject.getString("emailid");
                    str_username_fb = jsonObject.getString("username");
                    str_id_fb = jsonObject.getString("id");
                    Log.d("Tag", message);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (code.equals("0")) {
                SharedPreferences.Editor editor = getSharedPreferences(UtilsApi.MY_PREFS_NAME_FB, MODE_PRIVATE).edit();
                editor.putString("str_fbemailid", str_fbemailid);
                editor.putString("id", str_id_fb);
                editor.putString("uname", str_username_fb);
                editor.commit();
                Toast.makeText(LoginActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(), MainWithDrawerActivity.class);
                startActivity(intent);
                finish();
            } else if (code.equals("-1")) {
                Toast.makeText(LoginActivity.this, "" + message, Toast.LENGTH_SHORT).show();
            }
        }
    };
    private final Response.ErrorListener onPostsError = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.e("PostActivity", error.toString());
            if (hud != null) {
                hud.dismiss();
            }
            Toast.makeText(getApplicationContext(), "Internet Connection Error", Toast.LENGTH_SHORT).show();
        }
    };

    private void makeJsonArrayRequest(String id, final String emailid, String username, String imgUrl) {
        final KProgressHUD hud = KProgressHUD.create(LoginActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setMaxProgress(100)
                .show();
        String uri = "http://api.hashcrypt.com/api/facebook_login.php?fb_id=" + id + "&emailid=" + emailid + "&username=" + username;
        Log.d("Fb", uri);

        JsonArrayRequest request = new JsonArrayRequest(uri,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray jsonArray) {
                        if (hud != null) {
                            hud.dismiss();
                        }
                        Log.d("tag", "response :" + jsonArray.length());
                        for (int i = 0; i < jsonArray.length(); i++) {
                            try {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (hud != null) {
                            hud.dismiss();
                        }
                        error.printStackTrace();
                        Log.d("tag", "Error: " + error
                                + ">>" + error.networkResponse.statusCode
                                + ">>" + error.networkResponse.data
                                + ">>" + error.getCause()
                                + ">>" + error.getMessage());
                        Toast.makeText(LoginActivity.this, "Unable to fetch data: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        requestQueue.add(request);

//
//        JsonArrayRequest req = new JsonArrayRequest(uri,
//                new Response.Listener<JSONArray>() {
//                    @Override
//                    public void onResponse(JSONArray response) {
//                        Log.d("TAG", response.toString());
//                        Log.d("tag", "Response :" + response.toString());
//                        if (hud != null) {
//                            hud.dismiss();
//                        }
//                        Log.d("Tag", response.toString());
//                        String message = null;
//                        String code = null;
//                        Log.d("Tag", response.toString());
//                        try {
//                            //JSONArray array = new JSONArray(response);
//                            for (int i = 0; i < response.length(); i++) {
//                                JSONObject jsonObject = response.getJSONObject(i);
//                                message = jsonObject.getString("message");
//                                code = jsonObject.getString("code");
//                                str_fbemailid = jsonObject.getString("emailid");
//                                str_username_fb = jsonObject.getString("username");
//                                str_id_fb = jsonObject.getString("id");
//                                Log.d("Tag", message);
//
//                                Log.d("Tag", "Code:" + code);
//
//                                if (code.equals("0")) {
//                                    SharedPreferences.Editor editor = getSharedPreferences(UtilsApi.MY_PREFS_NAME_FB, MODE_PRIVATE).edit();
//                                    editor.putString("str_fbemailid", str_fbemailid);
//                                    editor.putString("id", str_id_fb);
//                                    editor.putString("uname", str_username_fb);
//                                    editor.commit();
//                                    Toast.makeText(LoginActivity.this, "" + message, Toast.LENGTH_SHORT).show();
//                                    LoginManager.getInstance().logOut();
//                                    Intent intent = new Intent(getApplicationContext(), MainWithDrawerActivity.class);
//                                    startActivity(intent);
//                                    finish();
//                                } else if (code.equals("-1")) {
//                                    Toast.makeText(LoginActivity.this, "" + message, Toast.LENGTH_SHORT).show();
//                                }
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                if (hud != null) {
//                    hud.dismiss();
//                }
//                LoginManager.getInstance().logOut();
//                VolleyLog.d("TAG", "Error: " + error.getMessage());
//                Log.d("tag", "Error :" + error.getMessage());
//                Toast.makeText(getApplicationContext(), "Internet Connection Error", Toast.LENGTH_SHORT).show();
//            }
//        });

        // Adding request to request queue
        ApplicationSantel.getInstance().addToRequestQueue(request);
    }

    private void loadFaceBookNew(String id, String emailid, String username, String imgUrl) {

        final KProgressHUD hud = KProgressHUD.create(LoginActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setMaxProgress(100)
                .show();
        String uri = UtilsApi.FACEB0OK_URL + "facebook_login.php?fb_id=" + id + "&emailid=" + emailid + "&username=" + username + "&photo=" + imgUrl;
        Log.d("Tag", uri);
//        StringRequest stringRequest = new StringRequest(Request.Method.GET,uri,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response)
//                    {
//                        if (hud != null) {
//                    hud.dismiss();
//                }
//                Log.d("Tag", response);
//                String message = null;
//                String code = null;
//                Log.d("Tag", response);
//                try {
//                    JSONArray array = new JSONArray(response);
//                    for (int i = 0; i < array.length(); i++) {
//                        JSONObject jsonObject = array.getJSONObject(i);
//                        message = jsonObject.getString("message");
//                        code = jsonObject.getString("code");
//                        str_fbemailid=jsonObject.getString("emailid");
//                        str_username_fb=jsonObject.getString("username");
//                        str_id_fb=jsonObject.getString("id");
//                        Log.d("Tag", message);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//                if (code.equals("0"))
//                {
//                    SharedPreferences.Editor editor = getSharedPreferences(UtilsApi.MY_PREFS_NAME_FB, MODE_PRIVATE).edit();
//                    editor.putString("str_fbemailid",str_fbemailid);
//                    editor.putString("id",str_id_fb);
//                    editor.putString("str_username_fb",str_username_fb);
//                    editor.commit();
//                    Toast.makeText(LoginActivity.this, "" + message, Toast.LENGTH_SHORT).show();
//                    Intent intent = new Intent(getApplicationContext(), MainWithDrawerActivity.class);
//                    startActivity(intent);
//                    finish();
//                } else if (code.equals("-1")) {
//                    Toast.makeText(LoginActivity.this, "" + message, Toast.LENGTH_SHORT).show();
//                }
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        if (hud != null) {
//                            hud.dismiss();
//                            error.printStackTrace();
//                        }
//                        Toast.makeText(LoginActivity.this, "Internet connection error", Toast.LENGTH_SHORT).show();
//                        //Toast.makeText(LoginActivity.this,error.getMessage(),Toast.LENGTH_LONG).show();
//                    }
//                });

//        RequestQueue requestQueue = Volley.newRequestQueue(this);
//        requestQueue.add(stringRequest);


        JsonArrayRequest req = new JsonArrayRequest(uri,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("TAG", response.toString());
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("TAG", "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        // Adding request to request queue
        ///AppController.getInstance().addToRequestQueue(req);
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(req);


//        final StringRequest request = new StringRequest(Request.Method.POST,uri, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                if (hud != null) {
//                    hud.dismiss();
//                }
//                Log.d("Tag", response);
//                String message = null;
//                String code = null;
//                Log.d("Tag", response);
//                try {
//                    JSONArray array = new JSONArray(response);
//                    for (int i = 0; i < array.length(); i++) {
//                        JSONObject jsonObject = array.getJSONObject(i);
//                        message = jsonObject.getString("message");
//                        code = jsonObject.getString("code");
//                        str_fbemailid=jsonObject.getString("emailid");
//                        str_username_fb=jsonObject.getString("username");
//                        str_id_fb=jsonObject.getString("id");
//                        Log.d("Tag", message);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//                if (code.equals("0"))
//                {
//                    SharedPreferences.Editor editor = getSharedPreferences(UtilsApi.MY_PREFS_NAME_FB, MODE_PRIVATE).edit();
//                    editor.putString("str_fbemailid",str_fbemailid);
//                    editor.putString("id",str_id_fb);
//                    editor.putString("str_username_fb",str_username_fb);
//                    editor.commit();
//                    Toast.makeText(LoginActivity.this, "" + message, Toast.LENGTH_SHORT).show();
//                    Intent intent = new Intent(getApplicationContext(), MainWithDrawerActivity.class);
//                    startActivity(intent);
//                    finish();
//                } else if (code.equals("-1")) {
//                    Toast.makeText(LoginActivity.this, "" + message, Toast.LENGTH_SHORT).show();
//                }
//            }
//        },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error)
//                    {
//                        if (hud != null) {
//                            hud.dismiss();
//                            error.printStackTrace();
//                        }
//                        Toast.makeText(LoginActivity.this, "Internet connection error", Toast.LENGTH_SHORT).show();
//                    }
//                });
//
//        RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);
//        queue.add(request);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_up:
                loadLogin();
                break;
            case R.id.txt_sing_up:
                Intent intent = new Intent(getApplicationContext(), RegitrationActivity.class);
                startActivityForResult(intent, UtilsApi.REQUESTCODE);
                break;
//            case R.id.relative_fb_login:
//                loingWithFB();
//
//                break;
            case R.id.txt_forgot_password:
                loadDialogForgotPassword();
                break;
            case R.id.txt_privacy_policy:
                Intent intent1 = new Intent(getApplicationContext(), PrivacyPolicyActivity.class);
                startActivityForResult(intent1, UtilsApi.REQUESTCODE);
                break;
        }
    }

    private void loadDialogForgotPassword() {
        final Dialog dialog = new Dialog(LoginActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.forgot_popup_layout);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView txt_cancel = (TextView) dialog.findViewById(R.id.txt_cancel);
        TextView txt_send = (TextView) dialog.findViewById(R.id.txt_send);
        final EditText edt_email = (EditText) dialog.findViewById(R.id.edt_email);

        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        txt_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str_email = edt_email.getText().toString().trim();
                loadForgotPassword(str_email);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void loadForgotPassword(String str_email) {

        final KProgressHUD hud = KProgressHUD.create(LoginActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setMaxProgress(100)
                .show();
        String uri = UtilsApi.BASE_URL + "forgot_password.php?emailid=" + str_email;
        StringRequest request = new StringRequest(uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (hud != null) {
                    hud.dismiss();
                }
                Log.d("Tag", response);
                String message = null;
                String code = null;
                Log.d("Tag", response);
                try {
                    JSONArray array = new JSONArray(response);
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsonObject = array.getJSONObject(i);
                        message = jsonObject.getString("message");
                        code = jsonObject.getString("code");
                        Log.d("Tag", message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (hud != null) {
                            hud.dismiss();
                        }
                        Toast.makeText(LoginActivity.this, "Internet connection error", Toast.LENGTH_SHORT).show();
                    }
                });
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void LoadFaceBook(String id, String emailid, String username, String photo) {
        final KProgressHUD hud = KProgressHUD.create(LoginActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setMaxProgress(100)
                .show();
        String uri = UtilsApi.FACEB0OK_URL + "facebook_login.php?fb_id=" + id + "&emailid=" + emailid + "&username=" + username + "&photo=" + photo;
        StringRequest request = new StringRequest(uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (hud != null) {
                    hud.dismiss();
                }
                Log.d("Tag", response);
                String message = null;
                String code = null;
                Log.d("Tag", response);
                try {
                    JSONArray array = new JSONArray(response);
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsonObject = array.getJSONObject(i);
                        message = jsonObject.getString("message");
                        code = jsonObject.getString("code");
                        str_fbemailid = jsonObject.getString("emailid");
                        str_username_fb = jsonObject.getString("username");
                        str_id_fb = jsonObject.getString("id");
                        Log.d("Tag", message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (code.equals("0")) {
                    SharedPreferences.Editor editor = getSharedPreferences(UtilsApi.MY_PREFS_NAME_FB, MODE_PRIVATE).edit();
                    editor.putString("str_fbemailid", str_fbemailid);
                    editor.putString("id", str_id_fb);
                    editor.putString("uname", str_username_fb);
                    editor.commit();
                    Toast.makeText(LoginActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), MainWithDrawerActivity.class);
                    startActivity(intent);
                    finish();
                } else if (code.equals("-1")) {
                    Toast.makeText(LoginActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (hud != null) {
                            hud.dismiss();
                        }
                        Toast.makeText(LoginActivity.this, "Internet connection error", Toast.LENGTH_SHORT).show();
                    }
                });
        RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);
        queue.add(request);
    }

    private void loadLogin() {
        String str_email = edt_email.getText().toString().trim();
        String str_password = edt_password.getText().toString().trim();
        String emailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        if (!str_email.isEmpty()) {
            if (str_email.matches(emailPattern)) {

                if (!str_password.isEmpty()) {
                    requestData(str_email, str_password);
                } else {
                    Toast.makeText(this, "Enter Password..", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getApplicationContext(), "Invalid email address", Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(this, "Enter Email..", Toast.LENGTH_SHORT).show();
        }
    }

    public void requestData(String str_email, String str_password) {
        final KProgressHUD hud = KProgressHUD.create(LoginActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setMaxProgress(100)
                .show();
        String uri = UtilsApi.LOGIN_URL + "login.php?emailid=" + str_email + "&password=" + str_password;
        final StringRequest request = new StringRequest(uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (hud != null) {
                    hud.dismiss();
                }
                String message = null;
                String code = null;
                Log.d("Tag", response);
                try {
                    JSONArray array = new JSONArray(response);
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsonObject = array.getJSONObject(i);
                        message = jsonObject.getString("message");
                        code = jsonObject.getString("code");
                        uname = jsonObject.getString("uname");
                        emailid = jsonObject.getString("emailid");
                        id = jsonObject.getString("id");
                        Log.d("Tag", message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (code.equals("0")) {
                    SharedPreferences.Editor editor = getSharedPreferences(UtilsApi.MY_PREFS_NAME_LOGIN, MODE_PRIVATE).edit();
                    editor.putString("uname", uname);
                    editor.putString("str_id", id);
                    editor.putString("emailid", emailid);
                    editor.commit();
                    Toast.makeText(LoginActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), MainWithDrawerActivity.class);
                    startActivity(intent);
                    finish();
                } else if (code.equals("-1")) {
                    Toast.makeText(LoginActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                } else if (code.equals("-2")) {
                    Toast.makeText(LoginActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (hud != null) {
                            hud.dismiss();
                        }
                        Toast.makeText(LoginActivity.this, "Internet connection error", Toast.LENGTH_SHORT).show();
                    }
                });

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }
}
