package com.grapes.santelquikeyy.Activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.grapes.santelquikeyy.Adapter.SparePartsAdapter;
import com.grapes.santelquikeyy.ItemClickSupport.ItemClickSupport;
import com.grapes.santelquikeyy.Model.ArrayListTempAcSub;
import com.grapes.santelquikeyy.Model.PriceTextServiceModel;
import com.grapes.santelquikeyy.Model.TypeOfServicesTwo;
import com.grapes.santelquikeyy.Model.TypeofPrice;
import com.grapes.santelquikeyy.R;
import com.grapes.santelquikeyy.UtilsApi;

import java.util.ArrayList;


/**
 * Created by pc on 8/2/2017.
 */

public class NewACServicesRepairActivity extends AppCompatActivity implements ObservableScrollViewCallbacks, View.OnClickListener {
    private View mImageView;
    private View mToolbarView;
    private ObservableScrollView mScrollView;
    private int mParallaxImageHeight;
    private Toolbar toolbar;
    private RecyclerView recyler_view_spare_parts;
    private ArrayList<Integer> arrayList_spare_part_image = new ArrayList<>();
    RelativeLayout relative_schedule_s_service;
    TextView txt_back_toolbar;

    public static ArrayList<TypeofPrice> typeofPriceArrayList = new ArrayList<>();

    public static ArrayList<PriceTextServiceModel> arrayList_show_display_service_name_1 = new ArrayList<>();
    public static ArrayList<String> arrayList_show_display_service_price_2 = new ArrayList<>();
    public static ArrayList<String> arrayList_service_check = new ArrayList<>();
//    public static ArrayList<TypeOfServicesTwo> arrayListTypeOfTwo = new ArrayList<>();

    public static String str_ac_value;

    public static String str_count = "1";
    public static ArrayList<String> arrayList_count = new ArrayList<>();
    //public static ArrayList<String>arrayList_temp=new ArrayList<>();
    public static ArrayList<String> arrayList_temp = new ArrayList<>();
    public static int finaltotal;
    public static int totalTemp;
    public static boolean check = true;
    public static String strradiobutton_value;
    public static int position = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_ac_services_and_repair_layout);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyler_view_spare_parts = (RecyclerView) findViewById(R.id.recyler_view_spare_parts);
        recyler_view_spare_parts.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyler_view_spare_parts.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1, GridLayoutManager.HORIZONTAL, false));
        recyler_view_spare_parts.setItemAnimator(new DefaultItemAnimator());

        arrayList_spare_part_image = getSpareImage();

        SparePartsAdapter adapter = new SparePartsAdapter(getApplicationContext(), arrayList_spare_part_image);
        recyler_view_spare_parts.setAdapter(adapter);
        ItemClickSupport.addTo(recyler_view_spare_parts)
                .setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                        Intent intent = new Intent(getApplicationContext(), SparePasrtClickActivity.class);
                        startActivityForResult(intent, 10001);
                    }
                });

        txt_back_toolbar = (TextView) toolbar.findViewById(R.id.txt_back_toolbar);
        txt_back_toolbar.setOnClickListener(this);
        relative_schedule_s_service = (RelativeLayout) findViewById(R.id.relative_schedule_s_service);
        relative_schedule_s_service.setOnClickListener(this);

        mImageView = findViewById(R.id.image);


        mToolbarView = findViewById(R.id.toolbar);
        mToolbarView.setBackgroundColor(ScrollUtils.getColorWithAlpha(0, getResources().getColor(R.color.colorPrimary)));

        mScrollView = (ObservableScrollView) findViewById(R.id.scroll);
        mScrollView.setScrollViewCallbacks(this);

        mParallaxImageHeight = getResources().getDimensionPixelSize(R.dimen.parallax_image_height);


        checkArray();
    }

    private void checkArray() {
        if (arrayList_count.size() > 0) {
            arrayList_count.clear();
        }
        if (arrayList_service_check.size() > 0) {
            arrayList_service_check.clear();
        }
        if (arrayList_show_display_service_name_1.size() > 0) {
            arrayList_show_display_service_name_1.clear();
        }
        if (arrayList_show_display_service_price_2.size() > 0) {
            arrayList_show_display_service_price_2.clear();
        }
    }

    private ArrayList<Integer> getSpareImage() {
        ArrayList<Integer> arrayList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            arrayList.add(R.drawable.ac_temp_spare_image);
        }
        return arrayList;
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        onScrollChanged(mScrollView.getCurrentScrollY(), false, false);
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        int baseColor = getResources().getColor(R.color.colorPrimary);
        float alpha = Math.min(1, (float) scrollY / mParallaxImageHeight);
        mToolbarView.setBackgroundColor(ScrollUtils.getColorWithAlpha(alpha, baseColor));
        // ViewHelper.setTranslationY(mImageView, scrollY / 2);
    }

    @Override
    public void onDownMotionEvent() {
    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.relative_schedule_s_service:
                Intent intent = new Intent(getApplicationContext(), Type_of_Ac_Activity.class);
                startActivityForResult(intent, UtilsApi.REQUESTCODE);
                //finish();
                break;
            case R.id.txt_back_toolbar:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent intent=new Intent(getApplicationContext(),MainWithDrawerActivity.class);
//        startActivityForResult(intent, UtilsApi.REQUESTCODE);
        finish();
    }
}
