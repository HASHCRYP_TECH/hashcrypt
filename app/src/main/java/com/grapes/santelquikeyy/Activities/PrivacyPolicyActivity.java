package com.grapes.santelquikeyy.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.grapes.santelquikeyy.R;
import com.grapes.santelquikeyy.UtilsApi;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pc on 7/22/2017.
 */

public class PrivacyPolicyActivity extends AppCompatActivity
{
    TextView txt_description,txt_title;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        setContentView(R.layout.privacy_policy_layout);
        initViews();
    }
    private void initViews() {

        txt_title=(TextView)findViewById(R.id.txt_title);
        txt_description=(TextView)findViewById(R.id.txt_description);
        Typeface tf_title = Typeface.createFromAsset(getApplicationContext().getAssets(),"font/OpenSans-Bold.ttf");
        Typeface tfdes = Typeface.createFromAsset(getApplicationContext().getAssets(),"font/OpenSans-Regular.ttf");
        txt_title.setTypeface(tf_title,Typeface.BOLD);
        txt_description.setTypeface(tfdes);
        loadPrivacy();
    }
    private void loadPrivacy()
    {
        final KProgressHUD hud = KProgressHUD.create(PrivacyPolicyActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setMaxProgress(100)
                .show();
        String uri=UtilsApi.BASE_URL+"cms.php?id="+1;
        StringRequest request = new StringRequest(uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {
                if (hud != null) {
                    hud.dismiss();
                }
                Log.d("Tag", response);
                String message = null;
                String code = null;
                Log.d("Tag", response);
                try {
                    JSONArray array = new JSONArray(response);
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsonObject = array.getJSONObject(i);
                        message = jsonObject.getString("message");
                        code = jsonObject.getString("code");
                        if (code.equals("0")) {
                            JSONArray ajproducts = jsonObject.getJSONArray("result");
                            for (int j = 0; j < ajproducts.length(); j++) {
                                JSONObject jsonObject1 = ajproducts.getJSONObject(j);
                                txt_title.setText(jsonObject1.getString("title"));
                                txt_description.setText(jsonObject1.getString("description"));
                            }
                        }
                        Log.d("Tag", message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (hud != null) {
                            hud.dismiss();
                        }
                        Toast.makeText(PrivacyPolicyActivity.this, "Connnection Error", Toast.LENGTH_SHORT).show();
                    }
                });
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
