package com.grapes.santelquikeyy.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.grapes.santelquikeyy.Activities.MainWithDrawerActivity;
import com.grapes.santelquikeyy.Model.TempAddsres;
import com.grapes.santelquikeyy.R;
import com.grapes.santelquikeyy.UtilsApi;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarListener;

/**
 * Created by pc on 8/5/2017.
 */

public class AddressActivity extends AppCompatActivity implements View.OnClickListener {
    private HorizontalCalendar horizontalCalendar;
    private RelativeLayout relative_schedule_s_service;
    private TextView txt_location_show, txt_back;
    private RelativeLayout relative_1, relative_2, relative_3, relative_4, relative_5, relative_6, relative_7, relative_8, relative_9, relative_10, relative_11;
    private RelativeLayout txt_location;
    private TextView txt_time_1, txt_time_2, txt_time_3, txt_time_4, txt_time_5, txt_time_6, txt_time_7, txt_time_8, txt_time_9, txt_time_10, txt_time_11;
    private TextView txt_nick;
    private ArrayList<TempAddsres> addsres = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.address_layout);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }


        /** end after 1 month from now */
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 1);

        /** start before 1 month from now */
        Calendar startDate = Calendar.getInstance();
        //startDate.add(Calendar.MONTH, -1);


        final Calendar defaultDate = Calendar.getInstance();
//        defaultDate.add(Calendar.MONTH, 1);
//        defaultDate.add(Calendar.DAY_OF_WEEK, +4);

        txt_location = (RelativeLayout) findViewById(R.id.txt_location);
        txt_location_show = (TextView) findViewById(R.id.txt_location_show);
        txt_location.setOnClickListener(this);
        txt_back = (TextView) findViewById(R.id.txt_back);
        txt_back.setOnClickListener(this);
        relative_1 = (RelativeLayout) findViewById(R.id.relative_1);
        relative_2 = (RelativeLayout) findViewById(R.id.relative_2);
        relative_3 = (RelativeLayout) findViewById(R.id.relative_3);
        relative_4 = (RelativeLayout) findViewById(R.id.relative_4);
        relative_5 = (RelativeLayout) findViewById(R.id.relative_5);
        relative_6 = (RelativeLayout) findViewById(R.id.relative_6);
        relative_7 = (RelativeLayout) findViewById(R.id.relative_7);
        relative_8 = (RelativeLayout) findViewById(R.id.relative_8);
        relative_9 = (RelativeLayout) findViewById(R.id.relative_9);
        relative_10 = (RelativeLayout) findViewById(R.id.relative_10);
        relative_11 = (RelativeLayout) findViewById(R.id.relative_11);

        txt_nick = (TextView) findViewById(R.id.txt_nick);

        String str_nick = getIntent().getStringExtra("str_nick_name");
        if (str_nick != null) {
            txt_nick.setText(str_nick);
            txt_nick.setVisibility(View.VISIBLE);
        }

        txt_time_1 = (TextView) findViewById(R.id.txt_time_1);
        txt_time_2 = (TextView) findViewById(R.id.txt_time_2);
        txt_time_3 = (TextView) findViewById(R.id.txt_time_3);
        txt_time_4 = (TextView) findViewById(R.id.txt_time_4);
        txt_time_5 = (TextView) findViewById(R.id.txt_time_5);
        txt_time_6 = (TextView) findViewById(R.id.txt_time_6);
        txt_time_7 = (TextView) findViewById(R.id.txt_time_7);
        txt_time_8 = (TextView) findViewById(R.id.txt_time_8);
        txt_time_9 = (TextView) findViewById(R.id.txt_time_9);
        txt_time_10 = (TextView) findViewById(R.id.txt_time_10);
        txt_time_11 = (TextView) findViewById(R.id.txt_time_11);


        relative_1.setOnClickListener(this);
        relative_2.setOnClickListener(this);
        relative_1.setOnClickListener(this);
        relative_3.setOnClickListener(this);
        relative_4.setOnClickListener(this);
        relative_5.setOnClickListener(this);
        relative_6.setOnClickListener(this);
        relative_7.setOnClickListener(this);
        relative_8.setOnClickListener(this);
        relative_9.setOnClickListener(this);
        relative_10.setOnClickListener(this);
        relative_11.setOnClickListener(this);

        txt_location_show.setText(getIntent().getStringExtra("str_address"));

        String strlist = getIntent().getStringExtra("str_address");
        TempAddsres tempAddsres = new TempAddsres();
        tempAddsres.setStr_address(strlist);
        String str_nick_1 = getIntent().getStringExtra("str_nick_name");
        if (str_nick != null) {
            tempAddsres.setStr_nick_name(str_nick_1);
        }
        addsres.add(tempAddsres);

        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Gson gson = new Gson();
        String json = appSharedPrefs.getString("TAG", "");
        try {
            JSONArray jsonArray = new JSONArray(json);
            for (int i = 0; i < jsonArray.length(); i++) {
                TempAddsres tempAddsres_1 = new TempAddsres();
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                tempAddsres_1.setStr_address(jsonObject.getString("str_address"));
                tempAddsres_1.setStr_nick_name(jsonObject.getString("str_nick_name"));
                addsres.add(tempAddsres_1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        SharedPreferences appSharedPrefssave = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor prefsEditor = appSharedPrefssave.edit();
        Gson gson2 = new Gson();
        String json2 = gson2.toJson(addsres);
        prefsEditor.putString("TAG", json2);
        prefsEditor.commit();
        horizontalCalendar = new HorizontalCalendar.Builder(this, R.id.calendarView)
                .startDate(startDate.getTime())
                // .endDate(endDate.getTime())
                .dayNameFormat("EEE")
                .dayNumberFormat("dd")
                .monthFormat("MMM")
                .showDayName(true)
                .showMonthName(true)
                .defaultSelectedDate(defaultDate.getTime())
                .textColor(Color.LTGRAY, Color.BLACK)
                .selectedDateBackground(Color.TRANSPARENT)
                .build();
        horizontalCalendar.goToday(false);
        relative_schedule_s_service = (RelativeLayout) findViewById(R.id.relative_schedule_s_service);
        relative_schedule_s_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), PricingDetailsActivity.class);
                startActivityForResult(intent, 1001);
            }
        });


        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Date date1, int position) {

                Date selectedDate = horizontalCalendar.getSelectedDate();
                // Date todatDate = Calendar.getInstance().getTime();

                Date todayDate = Calendar.getInstance().getTime();
                SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-mm-dd");
                String todayString = formatter1.format(todayDate);
                String todayStringSelected = formatter1.format(selectedDate);
//                String todayStringSelected = formatter1.format(date1);

                if (todayStringSelected.equals(todayString)) {

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/mm/dd HH:MM:SS");
                    String date = sdf.format(new Date());
                    Date testDate = null;
                    try {
                        testDate = sdf.parse(date);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    SimpleDateFormat formatter = new SimpleDateFormat("hh:mm:ss a");
                    String newFormat = formatter.format(testDate);
                    Log.e(" :: ", ".....Date... :  " + newFormat);

                    newFormat = newFormat.replace(":", "-");
                    newFormat = newFormat.replace(" ", ":");
                    String[] separated = newFormat.split(":");
                    String[] separated2 = newFormat.split("-");
                    Log.e(" :: ", ".....Date1... :  " + separated2[0]); // this will contain "Fruit"
                    Log.e(" :: ", ".....Date2... :  " + separated[1]); // this will contain " they taste good"

                    if (separated[1].toLowerCase().equalsIgnoreCase("pm")) {
                        relative_1.setVisibility(View.GONE);
                        relative_2.setVisibility(View.GONE);
                        if (Double.parseDouble(separated2[0]) < 1) {
                            relative_3.setVisibility(View.GONE);
                        } else if (Double.parseDouble(separated2[0]) < 2) {
                            relative_3.setVisibility(View.GONE);
                            relative_4.setVisibility(View.GONE);
                        } else if (Double.parseDouble(separated2[0]) < 3) {
                            relative_3.setVisibility(View.GONE);
                            relative_4.setVisibility(View.GONE);
                            relative_5.setVisibility(View.GONE);
                        } else if (Double.parseDouble(separated2[0]) < 4) {
                            relative_3.setVisibility(View.GONE);
                            relative_4.setVisibility(View.GONE);
                            relative_5.setVisibility(View.GONE);
                            relative_6.setVisibility(View.GONE);
                        } else if (Double.parseDouble(separated2[0]) < 5) {
                            relative_3.setVisibility(View.GONE);
                            relative_4.setVisibility(View.GONE);
                            relative_5.setVisibility(View.GONE);
                            relative_6.setVisibility(View.GONE);
                            relative_7.setVisibility(View.GONE);
                        } else if (Double.parseDouble(separated2[0]) < 6) {
                            relative_3.setVisibility(View.GONE);
                            relative_4.setVisibility(View.GONE);
                            relative_5.setVisibility(View.GONE);
                            relative_6.setVisibility(View.GONE);
                            relative_7.setVisibility(View.GONE);
                            relative_8.setVisibility(View.GONE);
                        } else if (Double.parseDouble(separated2[0]) < 7) {
                            relative_3.setVisibility(View.GONE);
                            relative_4.setVisibility(View.GONE);
                            relative_5.setVisibility(View.GONE);
                            relative_6.setVisibility(View.GONE);
                            relative_7.setVisibility(View.GONE);
                            relative_8.setVisibility(View.GONE);
                            relative_9.setVisibility(View.GONE);
                        } else if (Double.parseDouble(separated2[0]) < 8) {
                            relative_3.setVisibility(View.GONE);
                            relative_4.setVisibility(View.GONE);
                            relative_5.setVisibility(View.GONE);
                            relative_6.setVisibility(View.GONE);
                            relative_7.setVisibility(View.GONE);
                            relative_8.setVisibility(View.GONE);
                            relative_9.setVisibility(View.GONE);
                            relative_10.setVisibility(View.GONE);
                        } else if (Double.parseDouble(separated2[0]) < 8) {
                            relative_3.setVisibility(View.GONE);
                            relative_4.setVisibility(View.GONE);
                            relative_5.setVisibility(View.GONE);
                            relative_6.setVisibility(View.GONE);
                            relative_7.setVisibility(View.GONE);
                            relative_8.setVisibility(View.GONE);
                            relative_9.setVisibility(View.GONE);
                            relative_10.setVisibility(View.GONE);
                            relative_11.setVisibility(View.GONE);
                        } else {
                            relative_3.setVisibility(View.GONE);
                            relative_4.setVisibility(View.GONE);
                            relative_5.setVisibility(View.GONE);
                            relative_6.setVisibility(View.GONE);
                            relative_7.setVisibility(View.GONE);
                            relative_8.setVisibility(View.GONE);
                            relative_9.setVisibility(View.GONE);
                            relative_10.setVisibility(View.GONE);
                            relative_11.setVisibility(View.GONE);
                        }
                    } else if (Double.parseDouble(separated2[0]) == 10) {
                        relative_1.setVisibility(View.GONE);

                    } else if (Double.parseDouble(separated2[0]) == 11) {
                        relative_1.setVisibility(View.GONE);
                        relative_2.setVisibility(View.GONE);

                    }
                } else {
                    relative_1.setVisibility(View.VISIBLE);
                    relative_2.setVisibility(View.VISIBLE);
                    relative_3.setVisibility(View.VISIBLE);
                    relative_4.setVisibility(View.VISIBLE);
                    relative_5.setVisibility(View.VISIBLE);
                    relative_6.setVisibility(View.VISIBLE);
                    relative_7.setVisibility(View.VISIBLE);
                    relative_8.setVisibility(View.VISIBLE);
                    relative_9.setVisibility(View.VISIBLE);
                    relative_10.setVisibility(View.VISIBLE);
                    relative_11.setVisibility(View.VISIBLE);
                }
            }
        });


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/mm/dd HH:MM:SS");
        String date = sdf.format(new Date());
        Date testDate = null;
        try {
            testDate = sdf.parse(date);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        SimpleDateFormat formatter = new SimpleDateFormat("hh:mm:ss a");
        String newFormat = formatter.format(testDate);
        Log.e(" :: ", ".....Date... :  " + newFormat);

        newFormat = newFormat.replace(":", "-");
        newFormat = newFormat.replace(" ", ":");
        String[] separated = newFormat.split(":");
        String[] separated2 = newFormat.split("-");
        Log.e(" :: ", ".....Date1... :  " + separated2[0]); // this will contain "Fruit"
        Log.e(" :: ", ".....Date2... :  " + separated[1]); // this will contain " they taste good"

        if (separated[1].toLowerCase().equalsIgnoreCase("pm")) {
            relative_1.setVisibility(View.GONE);
            relative_2.setVisibility(View.GONE);
            if (Double.parseDouble(separated2[0]) < 1) {
                relative_3.setVisibility(View.GONE);
            } else if (Double.parseDouble(separated2[0]) < 2) {
                relative_3.setVisibility(View.GONE);
                relative_4.setVisibility(View.GONE);
            } else if (Double.parseDouble(separated2[0]) < 3) {
                relative_3.setVisibility(View.GONE);
                relative_4.setVisibility(View.GONE);
                relative_5.setVisibility(View.GONE);
            } else if (Double.parseDouble(separated2[0]) < 4) {
                relative_3.setVisibility(View.GONE);
                relative_4.setVisibility(View.GONE);
                relative_5.setVisibility(View.GONE);
                relative_6.setVisibility(View.GONE);
            } else if (Double.parseDouble(separated2[0]) < 5) {
                relative_3.setVisibility(View.GONE);
                relative_4.setVisibility(View.GONE);
                relative_5.setVisibility(View.GONE);
                relative_6.setVisibility(View.GONE);
                relative_7.setVisibility(View.GONE);
            } else if (Double.parseDouble(separated2[0]) < 6) {
                relative_3.setVisibility(View.GONE);
                relative_4.setVisibility(View.GONE);
                relative_5.setVisibility(View.GONE);
                relative_6.setVisibility(View.GONE);
                relative_7.setVisibility(View.GONE);
                relative_8.setVisibility(View.GONE);
            } else if (Double.parseDouble(separated2[0]) < 7) {
                relative_3.setVisibility(View.GONE);
                relative_4.setVisibility(View.GONE);
                relative_5.setVisibility(View.GONE);
                relative_6.setVisibility(View.GONE);
                relative_7.setVisibility(View.GONE);
                relative_8.setVisibility(View.GONE);
                relative_9.setVisibility(View.GONE);
            } else if (Double.parseDouble(separated2[0]) < 8) {
                relative_3.setVisibility(View.GONE);
                relative_4.setVisibility(View.GONE);
                relative_5.setVisibility(View.GONE);
                relative_6.setVisibility(View.GONE);
                relative_7.setVisibility(View.GONE);
                relative_8.setVisibility(View.GONE);
                relative_9.setVisibility(View.GONE);
                relative_10.setVisibility(View.GONE);
            } else if (Double.parseDouble(separated2[0]) < 8) {
                relative_3.setVisibility(View.GONE);
                relative_4.setVisibility(View.GONE);
                relative_5.setVisibility(View.GONE);
                relative_6.setVisibility(View.GONE);
                relative_7.setVisibility(View.GONE);
                relative_8.setVisibility(View.GONE);
                relative_9.setVisibility(View.GONE);
                relative_10.setVisibility(View.GONE);
                relative_11.setVisibility(View.GONE);
            } else {
                relative_3.setVisibility(View.GONE);
                relative_4.setVisibility(View.GONE);
                relative_5.setVisibility(View.GONE);
                relative_6.setVisibility(View.GONE);
                relative_7.setVisibility(View.GONE);
                relative_8.setVisibility(View.GONE);
                relative_9.setVisibility(View.GONE);
                relative_10.setVisibility(View.GONE);
                relative_11.setVisibility(View.GONE);
            }
        } else if (Double.parseDouble(separated2[0]) == 10) {
            relative_1.setVisibility(View.GONE);

        } else if (Double.parseDouble(separated2[0]) == 11) {
            relative_1.setVisibility(View.GONE);
            relative_2.setVisibility(View.GONE);

        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_location:
                Intent intent = new Intent(getApplicationContext(), AddNewAddressFinalActivity.class);
                intent.putExtra("str_address", txt_location_show.getText().toString().trim());
                startActivityForResult(intent, UtilsApi.REQUESTCODE);
                break;
            case R.id.relative_1:
                relative_1.setBackgroundResource(R.drawable.rounded_textview_time_black);
                txt_time_1.setTextColor(getResources().getColor(R.color.text_color_blue));
                relative_2.setBackgroundResource(R.drawable.rounded_time);
                relative_3.setBackgroundResource(R.drawable.rounded_time);
                relative_4.setBackgroundResource(R.drawable.rounded_time);
                relative_5.setBackgroundResource(R.drawable.rounded_time);
                relative_6.setBackgroundResource(R.drawable.rounded_time);
                relative_7.setBackgroundResource(R.drawable.rounded_time);
                relative_8.setBackgroundResource(R.drawable.rounded_time);
                relative_9.setBackgroundResource(R.drawable.rounded_time);
                relative_10.setBackgroundResource(R.drawable.rounded_time);
                relative_11.setBackgroundResource(R.drawable.rounded_time);

                // txt_time_1.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_2.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_3.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_4.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_5.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_6.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_7.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_8.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_9.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_10.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_11.setTextColor(getResources().getColor(R.color.text_color_gray));
                break;
            case R.id.relative_2:
                relative_1.setBackgroundResource(R.drawable.rounded_time);
                relative_2.setBackgroundResource(R.drawable.rounded_textview_time_black);
                txt_time_2.setTextColor(getResources().getColor(R.color.text_color_blue));
                relative_3.setBackgroundResource(R.drawable.rounded_time);
                relative_4.setBackgroundResource(R.drawable.rounded_time);
                relative_5.setBackgroundResource(R.drawable.rounded_time);
                relative_6.setBackgroundResource(R.drawable.rounded_time);
                relative_7.setBackgroundResource(R.drawable.rounded_time);
                relative_8.setBackgroundResource(R.drawable.rounded_time);
                relative_9.setBackgroundResource(R.drawable.rounded_time);
                relative_10.setBackgroundResource(R.drawable.rounded_time);
                relative_11.setBackgroundResource(R.drawable.rounded_time);

                txt_time_1.setTextColor(getResources().getColor(R.color.text_color_gray));
                // txt_time_2.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_3.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_4.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_5.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_6.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_7.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_8.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_9.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_10.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_11.setTextColor(getResources().getColor(R.color.text_color_gray));
                break;
            case R.id.relative_3:
                relative_1.setBackgroundResource(R.drawable.rounded_time);
                relative_2.setBackgroundResource(R.drawable.rounded_time);
                relative_3.setBackgroundResource(R.drawable.rounded_textview_time_black);
                txt_time_3.setTextColor(getResources().getColor(R.color.text_color_blue));
                relative_4.setBackgroundResource(R.drawable.rounded_time);
                relative_5.setBackgroundResource(R.drawable.rounded_time);
                relative_6.setBackgroundResource(R.drawable.rounded_time);
                relative_7.setBackgroundResource(R.drawable.rounded_time);
                relative_8.setBackgroundResource(R.drawable.rounded_time);
                relative_9.setBackgroundResource(R.drawable.rounded_time);
                relative_10.setBackgroundResource(R.drawable.rounded_time);
                relative_11.setBackgroundResource(R.drawable.rounded_time);

                txt_time_1.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_2.setTextColor(getResources().getColor(R.color.text_color_gray));
                //txt_time_3.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_4.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_5.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_6.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_7.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_8.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_9.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_10.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_11.setTextColor(getResources().getColor(R.color.text_color_gray));
                break;
            case R.id.relative_4:
                relative_1.setBackgroundResource(R.drawable.rounded_time);
                relative_2.setBackgroundResource(R.drawable.rounded_time);
                relative_3.setBackgroundResource(R.drawable.rounded_time);
                relative_4.setBackgroundResource(R.drawable.rounded_textview_time_black);
                txt_time_4.setTextColor(getResources().getColor(R.color.text_color_blue));
                relative_5.setBackgroundResource(R.drawable.rounded_time);
                relative_6.setBackgroundResource(R.drawable.rounded_time);
                relative_7.setBackgroundResource(R.drawable.rounded_time);
                relative_8.setBackgroundResource(R.drawable.rounded_time);
                relative_9.setBackgroundResource(R.drawable.rounded_time);
                relative_10.setBackgroundResource(R.drawable.rounded_time);
                relative_11.setBackgroundResource(R.drawable.rounded_time);

                txt_time_1.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_2.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_3.setTextColor(getResources().getColor(R.color.text_color_gray));
                // txt_time_4.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_5.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_6.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_7.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_8.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_9.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_10.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_11.setTextColor(getResources().getColor(R.color.text_color_gray));
                break;
            case R.id.relative_5:
                relative_1.setBackgroundResource(R.drawable.rounded_time);
                relative_2.setBackgroundResource(R.drawable.rounded_time);
                relative_3.setBackgroundResource(R.drawable.rounded_time);
                relative_4.setBackgroundResource(R.drawable.rounded_time);
                relative_5.setBackgroundResource(R.drawable.rounded_textview_time_black);
                txt_time_5.setTextColor(getResources().getColor(R.color.text_color_blue));
                relative_6.setBackgroundResource(R.drawable.rounded_time);
                relative_7.setBackgroundResource(R.drawable.rounded_time);
                relative_8.setBackgroundResource(R.drawable.rounded_time);
                relative_9.setBackgroundResource(R.drawable.rounded_time);
                relative_10.setBackgroundResource(R.drawable.rounded_time);
                relative_11.setBackgroundResource(R.drawable.rounded_time);

                txt_time_1.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_2.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_3.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_4.setTextColor(getResources().getColor(R.color.text_color_gray));
                // txt_time_5.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_6.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_7.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_8.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_9.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_10.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_11.setTextColor(getResources().getColor(R.color.text_color_gray));
                break;
            case R.id.relative_6:
                relative_1.setBackgroundResource(R.drawable.rounded_time);
                relative_2.setBackgroundResource(R.drawable.rounded_time);
                relative_3.setBackgroundResource(R.drawable.rounded_time);
                relative_4.setBackgroundResource(R.drawable.rounded_time);
                relative_5.setBackgroundResource(R.drawable.rounded_time);
                relative_6.setBackgroundResource(R.drawable.rounded_textview_time_black);
                txt_time_6.setTextColor(getResources().getColor(R.color.text_color_blue));
                relative_7.setBackgroundResource(R.drawable.rounded_time);
                relative_8.setBackgroundResource(R.drawable.rounded_time);
                relative_9.setBackgroundResource(R.drawable.rounded_time);
                relative_10.setBackgroundResource(R.drawable.rounded_time);
                relative_11.setBackgroundResource(R.drawable.rounded_time);

                txt_time_1.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_2.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_3.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_4.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_5.setTextColor(getResources().getColor(R.color.text_color_gray));
                //  txt_time_6.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_7.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_8.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_9.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_10.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_11.setTextColor(getResources().getColor(R.color.text_color_gray));
                break;
            case R.id.relative_7:
                relative_1.setBackgroundResource(R.drawable.rounded_time);
                relative_2.setBackgroundResource(R.drawable.rounded_time);
                relative_3.setBackgroundResource(R.drawable.rounded_time);
                relative_4.setBackgroundResource(R.drawable.rounded_time);
                relative_5.setBackgroundResource(R.drawable.rounded_time);
                relative_6.setBackgroundResource(R.drawable.rounded_time);
                relative_7.setBackgroundResource(R.drawable.rounded_textview_time_black);
                txt_time_7.setTextColor(getResources().getColor(R.color.text_color_blue));
                relative_8.setBackgroundResource(R.drawable.rounded_time);
                relative_9.setBackgroundResource(R.drawable.rounded_time);
                relative_10.setBackgroundResource(R.drawable.rounded_time);
                relative_11.setBackgroundResource(R.drawable.rounded_time);

                txt_time_1.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_2.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_3.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_4.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_5.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_6.setTextColor(getResources().getColor(R.color.text_color_gray));
                //txt_time_7.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_8.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_9.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_10.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_11.setTextColor(getResources().getColor(R.color.text_color_gray));
                break;
            case R.id.relative_8:
                relative_1.setBackgroundResource(R.drawable.rounded_time);
                relative_2.setBackgroundResource(R.drawable.rounded_time);
                relative_3.setBackgroundResource(R.drawable.rounded_time);
                relative_4.setBackgroundResource(R.drawable.rounded_time);
                relative_5.setBackgroundResource(R.drawable.rounded_time);
                relative_6.setBackgroundResource(R.drawable.rounded_time);
                relative_7.setBackgroundResource(R.drawable.rounded_time);
                relative_8.setBackgroundResource(R.drawable.rounded_textview_time_black);
                txt_time_8.setTextColor(getResources().getColor(R.color.text_color_blue));
                relative_9.setBackgroundResource(R.drawable.rounded_time);
                relative_10.setBackgroundResource(R.drawable.rounded_time);
                relative_11.setBackgroundResource(R.drawable.rounded_time);

                txt_time_1.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_2.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_3.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_4.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_5.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_6.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_7.setTextColor(getResources().getColor(R.color.text_color_gray));
                // txt_time_8.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_9.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_10.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_11.setTextColor(getResources().getColor(R.color.text_color_gray));
                break;
            case R.id.relative_9:
                relative_1.setBackgroundResource(R.drawable.rounded_time);
                relative_2.setBackgroundResource(R.drawable.rounded_time);
                relative_3.setBackgroundResource(R.drawable.rounded_time);
                relative_4.setBackgroundResource(R.drawable.rounded_time);
                relative_5.setBackgroundResource(R.drawable.rounded_time);
                relative_6.setBackgroundResource(R.drawable.rounded_time);
                relative_7.setBackgroundResource(R.drawable.rounded_time);
                relative_8.setBackgroundResource(R.drawable.rounded_time);
                relative_9.setBackgroundResource(R.drawable.rounded_textview_time_black);
                txt_time_9.setTextColor(getResources().getColor(R.color.text_color_blue));
                relative_10.setBackgroundResource(R.drawable.rounded_time);
                relative_11.setBackgroundResource(R.drawable.rounded_time);

                txt_time_1.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_2.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_3.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_4.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_5.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_6.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_7.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_8.setTextColor(getResources().getColor(R.color.text_color_gray));
                // txt_time_9.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_10.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_11.setTextColor(getResources().getColor(R.color.text_color_gray));
                break;
            case R.id.relative_10:
                relative_1.setBackgroundResource(R.drawable.rounded_time);
                relative_2.setBackgroundResource(R.drawable.rounded_time);
                relative_3.setBackgroundResource(R.drawable.rounded_time);
                relative_4.setBackgroundResource(R.drawable.rounded_time);
                relative_5.setBackgroundResource(R.drawable.rounded_time);
                relative_6.setBackgroundResource(R.drawable.rounded_time);
                relative_7.setBackgroundResource(R.drawable.rounded_time);
                relative_8.setBackgroundResource(R.drawable.rounded_time);
                relative_9.setBackgroundResource(R.drawable.rounded_time);
                relative_10.setBackgroundResource(R.drawable.rounded_textview_time_black);
                txt_time_10.setTextColor(getResources().getColor(R.color.text_color_blue));
                relative_11.setBackgroundResource(R.drawable.rounded_time);

                txt_time_1.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_2.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_3.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_4.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_5.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_6.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_7.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_8.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_9.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_11.setTextColor(getResources().getColor(R.color.text_color_gray));
                // txt_time_10.setTextColor(getResources().getColor(R.color.text_color_gray));
                break;
            case R.id.relative_11:
                relative_1.setBackgroundResource(R.drawable.rounded_time);
                relative_2.setBackgroundResource(R.drawable.rounded_time);
                relative_3.setBackgroundResource(R.drawable.rounded_time);
                relative_4.setBackgroundResource(R.drawable.rounded_time);
                relative_5.setBackgroundResource(R.drawable.rounded_time);
                relative_6.setBackgroundResource(R.drawable.rounded_time);
                relative_7.setBackgroundResource(R.drawable.rounded_time);
                relative_8.setBackgroundResource(R.drawable.rounded_time);
                relative_9.setBackgroundResource(R.drawable.rounded_time);
                relative_10.setBackgroundResource(R.drawable.rounded_time);
                relative_11.setBackgroundResource(R.drawable.rounded_textview_time_black);

                txt_time_1.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_2.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_3.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_4.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_5.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_6.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_7.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_8.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_9.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_10.setTextColor(getResources().getColor(R.color.text_color_gray));
                txt_time_11.setTextColor(getResources().getColor(R.color.text_color_blue));
                break;
            case R.id.txt_back:
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        if (UtilsApi.REQUESTCODE == requestCode) {
            if (AddNewAddressFinalActivity.str_address != null) {
                txt_location_show.setText(AddNewAddressFinalActivity.str_address);
            }
            if (AddNewAddressFinalActivity.str_nick_name != null) {
                txt_nick.setText(AddNewAddressFinalActivity.str_nick_name);
                txt_nick.setVisibility(View.VISIBLE);
            }
        }
    }
}
