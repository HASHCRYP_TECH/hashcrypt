package com.grapes.santelquikeyy.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.grapes.santelquikeyy.Adapter.DesReviewAdapter;
import com.grapes.santelquikeyy.Adapter.OfferAdapter;
import com.grapes.santelquikeyy.Adapter.ProductAdapter;
import com.grapes.santelquikeyy.Model.BannerOffers;
import com.grapes.santelquikeyy.Model.Offer;
import com.grapes.santelquikeyy.Model.Products;
import com.grapes.santelquikeyy.R;
import com.grapes.santelquikeyy.UtilsApi;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pc on 7/19/2017.
 */

public class MainActivity extends AppCompatActivity
{
    private RecyclerView recyler_View_offer,recyler_View_product,recyler_View_Review;
    private OfferAdapter mAdapter;
    private ProductAdapter mProductAdapter;
    private DesReviewAdapter mDesReviewAdapter;
    private List<Offer>offerList_Image=new ArrayList<>();
    private List<Offer>product_Image=new ArrayList<>();
    private List<Offer>des_review_Image=new ArrayList<>();
    private SearchView searview_santel;
    private List<BannerOffers>bannerOffersList=new ArrayList<>();
    private List<Products>productsList=new ArrayList<>();
    private TextView txt_monsoon;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_offers);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));

    }
        initViews();
    }
    private void initViews()
    {
        txt_monsoon=(TextView)findViewById(R.id.txt_monsoon);
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(),"font/OpenSans-Bold.ttf");
        txt_monsoon.setTypeface(tf,Typeface.BOLD);
        searview_santel=(SearchView)findViewById(R.id.searview_santel);
        searview_santel.setIconifiedByDefault(false);

        ///searview_santel.setOnQueryTextListener(this);
        //searview_santel.setSubmitButtonEnabled(false);
        searview_santel.setQueryHint("What are you looking");
        Typeface tfdes = Typeface.createFromAsset(getApplicationContext().getAssets(),"font/OpenSans-Regular.ttf");
        TextView searchText = (TextView)
                searview_santel.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchText.setTypeface(tfdes);
        InputMethodManager imm1 = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm1.showSoftInput(searview_santel, InputMethodManager.SHOW_FORCED);
        getOfferImage();
    }
    private void getOfferImage()
    {
        final KProgressHUD hud = KProgressHUD.create(MainActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setMaxProgress(100)
                .show();
        String uri = UtilsApi.BANNEROFFERS;
        final StringRequest request = new StringRequest(uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (hud != null) {
                    hud.dismiss();
                }
                Log.d("Tag", response);
                String message = null;
                String code = null;


                Log.d("Tag", response);
                try {
                    JSONArray jsonArray=new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject=jsonArray.getJSONObject(i);
                        code=jsonObject.getString("code");
                        message=jsonObject.getString("message");
                        if (code.equals("0")) {
                            JSONArray phoneArray = jsonObject.getJSONArray("result");
                            for (int j = 0; j < phoneArray.length(); j++) {
                                JSONObject jsonObject1 = phoneArray.getJSONObject(j);
                                BannerOffers bannerOffers=new BannerOffers();
                                bannerOffers.setId(jsonObject1.getString("id"));
                                bannerOffers.setTitle(jsonObject1.getString("title"));
                                bannerOffers.setImage(jsonObject1.getString("image"));
                                bannerOffersList.add(bannerOffers);
                            }
                        }
                    }
                    if (bannerOffersList.size()>0) {
                        recyler_View_offer = (RecyclerView) findViewById(R.id.recyler_View_offer);
                        mAdapter = new OfferAdapter(getApplicationContext(), bannerOffersList);
                        recyler_View_offer.setHasFixedSize(true);
                        DividerItemDecoration divider = new DividerItemDecoration(recyler_View_offer.getContext(), DividerItemDecoration.VERTICAL);
                        divider.setDrawable(ContextCompat.getDrawable(getBaseContext(), R.drawable.custom_divider));
                        recyler_View_offer.addItemDecoration(divider);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                        recyler_View_offer.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1, GridLayoutManager.HORIZONTAL, false));
                        recyler_View_offer.setItemAnimator(new DefaultItemAnimator());
                        recyler_View_offer.setAdapter(mAdapter);
                    }
                    loadProduct();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (hud != null) {
                            hud.dismiss();
                        }
                        Toast.makeText(MainActivity.this, "Connnection Error", Toast.LENGTH_SHORT).show();
                    }
                });
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);



    }
    private void loadProduct()
    {

        final KProgressHUD hud = KProgressHUD.create(MainActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setMaxProgress(100)
                .show();
        String uri = UtilsApi.PRODUCT;
        final StringRequest request = new StringRequest(uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (hud != null) {
                    hud.dismiss();
                }
                Log.d("Tag", response);
                String message = null;
                String code = null;


                Log.d("Tag", response);
                try {
                    JSONArray jsonArray=new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject=jsonArray.getJSONObject(i);
                        code=jsonObject.getString("code");
                        message=jsonObject.getString("message");
                        if (code.equals("0")) {
                            JSONArray ajproducts = jsonObject.getJSONArray("result");
                            for (int j = 0; j < ajproducts.length(); j++) {
                                JSONObject jsonObject1 = ajproducts.getJSONObject(j);
                                Products products=new Products();
                                products.setId(jsonObject1.getString("id"));
                                products.setTitle(jsonObject1.getString("title"));
                                products.setImage(jsonObject1.getString("image"));
                                products.setDescription(jsonObject1.getString("description"));
                                products.setPrice(jsonObject1.getString("price"));
                                productsList.add(products);
                            }
                        }
                    }
                    if (productsList.size()>0) {
                        recyler_View_product=(RecyclerView)findViewById(R.id.recyler_View_product);
                        mProductAdapter=new ProductAdapter(getApplicationContext(),productsList);
                        recyler_View_product.setHasFixedSize(true);
                        DividerItemDecoration divider = new DividerItemDecoration(recyler_View_product.getContext(), DividerItemDecoration.VERTICAL);
                        divider.setDrawable(ContextCompat.getDrawable(getBaseContext(), R.drawable.custom_divider));
                        recyler_View_product.addItemDecoration(divider);
                        recyler_View_product.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1, GridLayoutManager.HORIZONTAL, false));
                        recyler_View_product.setItemAnimator(new DefaultItemAnimator());
                        recyler_View_product.setAdapter(mProductAdapter);
                    }
                    loadReview();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (hud != null) {
                            hud.dismiss();
                        }
                        Toast.makeText(MainActivity.this, "Connnection Error", Toast.LENGTH_SHORT).show();
                    }
                });
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);


    }
    private List<Offer> getproduct() {
        List<Offer>offerList=new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Offer offer=new Offer();
            offer.setOfferimage(R.drawable.ac_png_picture);
            offerList.add(offer);
        }
        return offerList;
    }
    private void loadReview() {
        des_review_Image=getReviewImage();

        recyler_View_Review=(RecyclerView)findViewById(R.id.recyler_View_Review);
        recyler_View_Review.setHasFixedSize(true);
        DividerItemDecoration horizontalDecoration = new DividerItemDecoration(recyler_View_Review.getContext(),
                DividerItemDecoration.HORIZONTAL);
        Drawable horizontalDivider = ContextCompat.getDrawable(getApplicationContext(), R.drawable.horizontal_divider);
        horizontalDecoration.setDrawable(horizontalDivider);
        recyler_View_Review.addItemDecoration(horizontalDecoration);
        mDesReviewAdapter=new DesReviewAdapter(getApplicationContext(),des_review_Image);
        recyler_View_Review.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1, GridLayoutManager.HORIZONTAL, false));
        recyler_View_Review.setItemAnimator(new DefaultItemAnimator());
        recyler_View_Review.setAdapter(mDesReviewAdapter);
    }
    private List<Offer> getReviewImage()
    {
        List<Offer>offerList=new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Offer offer=new Offer();
            offer.setOfferimage(R.drawable.user_review_icon);
            offerList.add(offer);
        }
        return offerList;
    }
}
