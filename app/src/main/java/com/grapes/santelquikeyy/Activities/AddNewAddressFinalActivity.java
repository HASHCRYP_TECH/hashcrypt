package com.grapes.santelquikeyy.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.grapes.santelquikeyy.Adapter.AddressListAdapter;
import com.grapes.santelquikeyy.Model.TempAddsres;
import com.grapes.santelquikeyy.R;
import com.grapes.santelquikeyy.UtilsApi;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by pc on 8/9/2017.
 */

public class AddNewAddressFinalActivity extends AppCompatActivity implements View.OnClickListener {
    RelativeLayout relative_add_address;
    RadioButton rd_button;
    TextView txt_back;
    ArrayList<TempAddsres>addsresArrayList=new ArrayList<>();
    ListView list_item;
    public static String str_address;
    public static String str_nick_name;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_new_layout_final_change_layout);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }
        iniViews();
    }
    private void iniViews() {
        txt_back=(TextView)findViewById(R.id.txt_back);
        txt_back.setOnClickListener(this);
        relative_add_address=(RelativeLayout)findViewById(R.id.relative_add_address);
        relative_add_address.setOnClickListener(this);
        list_item=(ListView)findViewById(R.id.list_item);
        list_item.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                str_address=addsresArrayList.get(position).getStr_address();
                str_nick_name=addsresArrayList.get(position).getStr_nick_name();
                Toast.makeText(AddNewAddressFinalActivity.this, "Click", Toast.LENGTH_SHORT).show();
                Intent intent=new Intent();
                intent.putExtra("str_name","");
                setResult(RESULT_OK,intent);
                finish();
            }
        });
        final SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Gson gson = new Gson();
        String json = appSharedPrefs.getString("TAG", "");
        try {
            JSONArray jsonArray=new JSONArray(json);
            for (int i = 0; i < jsonArray.length(); i++)
            {
                TempAddsres tempAddsres=new TempAddsres();
                JSONObject jsonObject=jsonArray.getJSONObject(i);
                tempAddsres.setStr_address(jsonObject.getString("str_address"));
                tempAddsres.setStr_nick_name(jsonObject.getString("str_nick_name"));
                addsresArrayList.add(tempAddsres);
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }

        if (addsresArrayList.size()>0)
        {
            AddressListAdapter addressListAdapter=new AddressListAdapter(AddNewAddressFinalActivity.this,addsresArrayList);
            list_item.setAdapter(addressListAdapter);
        }



    }
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.relative_add_address:
                Intent intent=new Intent(getApplicationContext(),Add_New_Activity.class);
                startActivityForResult(intent, UtilsApi.REQUESTCODE);
                break;
            case R.id.txt_back:
                onBackPressed();
                break;
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
