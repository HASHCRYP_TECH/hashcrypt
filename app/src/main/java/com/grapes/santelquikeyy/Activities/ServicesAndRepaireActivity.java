package com.grapes.santelquikeyy.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.RelativeLayout;

import com.grapes.santelquikeyy.R;
import com.grapes.santelquikeyy.UtilsApi;

/**
 * Created by pc on 7/31/2017.
 */

public class ServicesAndRepaireActivity extends AppCompatActivity
{
    RelativeLayout relative_schedule_s_service;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_services_and_repaire_and_layout);

        iniViews();
    }

    private void iniViews() {
        Intent intent=new Intent(getApplicationContext(),Type_of_Ac_Activity.class);
        startActivityForResult(intent, UtilsApi.REQUESTCODE);
    }
}
