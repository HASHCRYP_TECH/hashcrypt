package com.grapes.santelquikeyy.Activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.grapes.santelquikeyy.Adapter.SparePartsAdapter;
import com.grapes.santelquikeyy.ItemClickSupport.ItemClickSupport;
import com.grapes.santelquikeyy.R;
import com.grapes.santelquikeyy.UtilsApi;

import java.util.ArrayList;

/**
 * Created by pc on 8/2/2017.
 */

public class SparePasrtClickActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView img_spare_parts;
    RecyclerView recyler_view_spare_parts;
    TextView txt_back;
    private ArrayList<Integer> arrayList_spare_part_image=new ArrayList<>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.spare_parts_click_image_layout);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.spare_color_black));
        }

        intiViews();
    }

    private void intiViews()
    {

        img_spare_parts=(ImageView)findViewById(R.id.img_spare_parts);
        img_spare_parts.setBackgroundResource(R.drawable.ac_temp_spare_image);
        recyler_view_spare_parts=(RecyclerView)findViewById(R.id.recyler_view_spare_parts);
        txt_back=(TextView)findViewById(R.id.txt_back);
        txt_back.setOnClickListener(this);
        recyler_view_spare_parts.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyler_view_spare_parts.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1, GridLayoutManager.HORIZONTAL, false));
        recyler_view_spare_parts.setItemAnimator(new DefaultItemAnimator());
        arrayList_spare_part_image=getSpareImage();
        SparePartsAdapter adapter=new SparePartsAdapter(getApplicationContext(),arrayList_spare_part_image);
        recyler_view_spare_parts.setAdapter(adapter);
        ItemClickSupport.addTo(recyler_view_spare_parts)
                .setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                        img_spare_parts.setBackgroundResource(arrayList_spare_part_image.get(position));
                    }
                });

    }
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.txt_back:
                onBackPressed();
                break;
        }
    }
    private ArrayList<Integer> getSpareImage()
    {
        ArrayList<Integer>arrayList=new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            arrayList.add(R.drawable.ac_temp_spare_image);
        }
        return arrayList;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
