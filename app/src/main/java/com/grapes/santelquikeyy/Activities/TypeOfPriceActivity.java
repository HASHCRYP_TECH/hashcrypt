package com.grapes.santelquikeyy.Activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.grapes.santelquikeyy.Adapter.PersonAdapter;
import com.grapes.santelquikeyy.Adapter.TypeRec_of_Price_Adapter;
import com.grapes.santelquikeyy.Model.PriceTextServiceModel;
import com.grapes.santelquikeyy.Model.TypeofPrice;
import com.grapes.santelquikeyy.R;
import com.grapes.santelquikeyy.UtilsApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by pc on 7/31/2017.
 */

public class TypeOfPriceActivity extends AppCompatActivity implements View.OnClickListener {
    TextView txt_complete, txt_what_type;
    ProgressBar progress_bar;

    //  ListView list_item;
    TextView txt_back, txt_cancel;
    String str_w_id;
    public static RelativeLayout relative_done;
    RecyclerView recyler_type_service;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.guide_text_color));
        }
        setContentView(R.layout.type_of_price_layout);
        initViews();
    }

    private void initViews() {
        relative_done = (RelativeLayout) findViewById(R.id.relative_done);
        relative_done.setOnClickListener(this);
        str_w_id = getIntent().getStringExtra("str_window");
        if (str_w_id != null) {
            NewACServicesRepairActivity.str_ac_value = str_w_id;
        }
        txt_cancel = (TextView) findViewById(R.id.txt_cancel);
        txt_cancel.setOnClickListener(this);
        txt_back = (TextView) findViewById(R.id.txt_back);
        txt_back.setOnClickListener(this);
        txt_complete = (TextView) findViewById(R.id.txt_complete);
        txt_what_type = (TextView) findViewById(R.id.txt_what_type);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
        /// list_item=(ListView)findViewById(R.id.list_item);
        recyler_type_service = (RecyclerView) findViewById(R.id.recyler_type_service);
        if (NewACServicesRepairActivity.typeofPriceArrayList.size() > 0) {
            NewACServicesRepairActivity.typeofPriceArrayList.clear();
        }
        Load_Type_Services();
    }

    public void Load_Type_Services() {
//        final KProgressHUD hud = KProgressHUD.create(TypeOfPriceActivity.this)
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                .setLabel("Please wait")
//                .setMaxProgress(100)
//                .show();
        String uri = UtilsApi.Service_Price + "services_price.php?type=1&id=" + NewACServicesRepairActivity.str_ac_value;
        StringRequest request = new StringRequest(uri, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                if (hud != null) {
//                    hud.dismiss();
//                }
                Log.d("Tag", response);

                String code = null;
                String message = null;
                //String complete=null;
                String complete;
                try {
                    JSONArray array = new JSONArray(response);
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsonObject = array.getJSONObject(i);
                        code = jsonObject.getString("code");
                        //message=jsonObject.getString("message");
                        complete = jsonObject.getString("complete");
                        if (code.equals("0")) {
                            txt_complete.setText(complete + " " + "Complete");
                            String result = complete.substring(0, complete.length() - 1);
                            progress_bar.setProgress(Integer.parseInt(result));
                            txt_what_type.setText(message);
                        }
                        if (code.equals("0")) {
                            JSONArray phoneArray = jsonObject.getJSONArray("result");
                            for (int j = 0; j < phoneArray.length(); j++) {
                                JSONObject jsonObject1 = phoneArray.getJSONObject(j);
                                TypeofPrice typeService = new TypeofPrice();
                                typeService.setId(jsonObject1.getString("id"));
                                typeService.setTitle(jsonObject1.getString("title"));
                                typeService.setName(jsonObject1.getString("name"));

                                String str_name = jsonObject1.getString("name");
                                String sr_name = jsonObject1.getString("name");
                                String substr = str_name.substring(str_name.indexOf(str_name) + 5);
                                String desiredString = sr_name.substring(0, 1);
                                typeService.setStr_number_of_service(desiredString);
                                typeService.setStr_price(substr);
                                NewACServicesRepairActivity.typeofPriceArrayList.add(typeService);
                            }
                        }
                        if (NewACServicesRepairActivity.typeofPriceArrayList.size() > 0) {
                            txt_what_type.setText(NewACServicesRepairActivity.typeofPriceArrayList.get(0).getTitle());
                            recyler_type_service.setHasFixedSize(true);
                            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                            recyler_type_service.setLayoutManager(layoutManager);
                            recyler_type_service.setAdapter(new PersonAdapter(getApplicationContext(), NewACServicesRepairActivity.typeofPriceArrayList));
//                            TypeRec_of_Price_Adapter typeOfPriceAdapter=new TypeRec_of_Price_Adapter(getApplicationContext(),NewACServicesRepairActivity.typeofPriceArrayList);
//                            recyler_type_service.setAdapter(typeOfPriceAdapter);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (code.equals("0")) {

//                    Intent intent = new Intent(getApplicationContext(), MainWithDrawerActivity.class);
//                    startActivity(intent);
                } else if (code.equals("-1")) {
                    Toast.makeText(TypeOfPriceActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        if (hud != null) {
//                            hud.dismiss();
//                        }
                        Toast.makeText(TypeOfPriceActivity.this, "Internet connection error", Toast.LENGTH_SHORT).show();
                    }
                });
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent intent=new Intent(getApplicationContext(),TypeAcSubActivity.class);
//        intent.putExtra("str_w_id",str_w_id);
//        startActivityForResult(intent,UtilsApi.REQUESTCODE);
        finish();
        // NewACServicesRepairActivity.typeofPriceArrayList.clear();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_back:
                onBackPressed();
                break;
            case R.id.txt_cancel:
                Intent intent1 = new Intent(getApplicationContext(), MainWithDrawerActivity.class);
                startActivityForResult(intent1, UtilsApi.REQUESTCODE);
                //  finish();
                break;
            case R.id.relative_done:
                loadMain();
                break;
        }
    }

    private void loadMain() {
        ArrayList<String> arrayList = new ArrayList<>();
        StringBuilder stringBuilder = new StringBuilder();
        String str_w_id = "", str_s_id;
        for (TypeofPrice appList : NewACServicesRepairActivity.typeofPriceArrayList) {
            if (appList.isSelected())
            {
                appList.setSelected(false);
                if (stringBuilder.length() > 0)
                    stringBuilder.append(", ");
                stringBuilder.append(appList.getNumberType());
                int position = appList.getNumberType();
                str_w_id = NewACServicesRepairActivity.typeofPriceArrayList.get(position).getId();
                str_s_id = NewACServicesRepairActivity.typeofPriceArrayList.get(position).getId();
                arrayList.add(str_w_id);
            }
        }
        if (arrayList.size() > 0) {

            if (NewACServicesRepairActivity.arrayList_count.size() > 0) {
                if (NewACServicesRepairActivity.arrayList_service_check.size() > 0) {
                    NewACServicesRepairActivity.arrayList_service_check.remove(0);

                    NewACServicesRepairActivity.arrayList_count.remove(0);
                }
                if (NewACServicesRepairActivity.arrayList_service_check.size() > 0) {
                    String size = NewACServicesRepairActivity.arrayList_count.get(0);
                    NewACServicesRepairActivity.str_count = size;
                    NewACServicesRepairActivity.check = true;

                    String str_service_name = NewACServicesRepairActivity.arrayList_show_display_service_name_1.get(NewACServicesRepairActivity.position).getStr_sevice_name();
                    PriceTextServiceModel priceTextServiceModel = new PriceTextServiceModel();
                    priceTextServiceModel.setStr_price(NewACServicesRepairActivity.strradiobutton_value);
                    priceTextServiceModel.setStr_sevice_name(str_service_name);
                    NewACServicesRepairActivity.arrayList_show_display_service_name_1.set(NewACServicesRepairActivity.position, priceTextServiceModel);
                    NewACServicesRepairActivity.check = true;
                    NewACServicesRepairActivity.position = +1;
                    Intent intent = new Intent(getApplicationContext(), TypeOfPriceActivity.class);
                    startActivityForResult(intent, UtilsApi.REQUESTCODE);
                    // finish();
                } else {
                    if (UtilsApi.arrayList_id.size() > 0) {
                        UtilsApi.arrayList_id.remove(0);
                    }
                    if (UtilsApi.arrayList_id.size() > 0) {
//                        for (int i = 0; i < NewACServicesRepairActivity.arrayList_show_display_service_name_1.size(); i++) {
//                            String str_price_null=NewACServicesRepairActivity.arrayList_show_display_service_name_1.get(i).getStr_price();
//                            String str_service_name=NewACServicesRepairActivity.arrayList_show_display_service_name_1.get(i).getStr_sevice_name();
//
//                            if (str_price_null==null);
//                            {
//                                PriceTextServiceModel priceTextServiceModel=new PriceTextServiceModel();
//                                priceTextServiceModel.setStr_price(NewACServicesRepairActivity.strradiobutton_value);
//                                priceTextServiceModel.setStr_sevice_name(str_service_name);
//                                NewACServicesRepairActivity.arrayList_show_display_service_name_1.set(i,priceTextServiceModel);
//                            }
//
//                        }
                        // String str_price_null = NewACServicesRepairActivity.arrayList_show_display_service_name_1.get(NewACServicesRepairActivity.position).getStr_price();

                        Intent intent = new Intent(getApplicationContext(), TypeAcSubActivity.class);
                        startActivityForResult(intent, UtilsApi.REQUESTCODE);
                        // finish();
                    } else {
                        Intent intent = new Intent(getApplicationContext(), Add_AddressActivity.class);
                        startActivityForResult(intent, UtilsApi.REQUESTCODE);
                        // finish();
                    }
//                    Intent intent = new Intent(getApplicationContext(), Add_AddressActivity.class);
//                    startActivityForResult(intent, UtilsApi.REQUESTCODE);
                    // finish();
                }
            } else {


                if (UtilsApi.arrayList_id.size() > 0) {
                    UtilsApi.arrayList_id.remove(0);
                }
                if (UtilsApi.arrayList_id.size() > 0) {
                    Intent intent = new Intent(getApplicationContext(), TypeAcSubActivity.class);
                    startActivityForResult(intent, UtilsApi.REQUESTCODE);
                    // finish();
                } else {
                    Intent intent = new Intent(getApplicationContext(), Add_AddressActivity.class);
                    startActivityForResult(intent, UtilsApi.REQUESTCODE);
                    // finish();
                }

//                if (UtilsApi.arrayList_id.size() > 0)
//                {
//                    UtilsApi.arrayList_id.remove(0);
//                }
//                if (UtilsApi.arrayList_id.size() > 0) {
//                    Intent intent = new Intent(getApplicationContext(), TypeAcSubActivity.class);
//                    startActivityForResult(intent, UtilsApi.REQUESTCODE);
//                    // finish();
//                } else {
//                    Intent intent = new Intent(getApplicationContext(), Add_AddressActivity.class);
//                    startActivityForResult(intent, UtilsApi.REQUESTCODE);
//                    // finish();
//                }
            }
        } else {
            Toast.makeText(this, "Please select price..", Toast.LENGTH_SHORT).show();
        }
    }
}
