package com.grapes.santelquikeyy.api;

import com.google.android.gms.vision.face.Face;
import com.grapes.santelquikeyy.responseModel.FacebookModel;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by Hitesh on 08/19/2017.
 */

public interface ApiInterface {

    @GET("api/facebook_login.php")
    Call<List<FacebookModel>> getFacebookResponse(@Query("fb_id") String fb_id, @Query("emailid") String emailId, @Query("username") String username);

}
